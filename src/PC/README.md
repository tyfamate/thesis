# PC side for enrollment

In this folder you can find a simple [bash script](./enrollment.sh).

You can use it to connect to the device and perform the enrollment procedure.
It can automatically sign the CSR using an openssl certificate authority
that is in [CA](./CA) folder.

To enroll a device that is connected on USB0 simply run:

`./enrollment.sh /dev/ttyUSB0`

You can also modify the script to automatically sign using another CA.

If you modify DEBUG_SCRIPT and/or DEBUG_ESP, variables inside the script, you
can also get debugging information (if enabled by the esp32).

It must be noted that the script was inteded to be used without debuggin information from
the device.
It might or might not work.


# To sign CSR:
`openssl ca -config CA/openssl.cnf -days 3650 -notext -md sha256 -in generatedCSR.pem -out generatedCERT.pem`

## CA key paraphrase
`CApassword`

# To verify a certificate:
`openssl verify -CAfile CA/certs/tyfamateCA_cert.pem generatedCERT.pem`
