#!/bin/bash
#######################################################
# Helper script for device enrollment.                #
#                                                     #
# @author  Matěj Týfa (tyfamate)                      #
# @date    2022-03-14                                 #
# tyfamate@fit.cvut.cz                                #
#                                                     #
# Created as part of my Bachelor's thesis:            #
# Using physical unclonable functions in TLS on ESP32 #
# Czech Technical University in Prague,               #
# Faculty of Information Technology,                  #
# Department of Computer Systems                      #
# 2022                                                #
#######################################################

######################################################################################################################
# USAGE
######################################################################################################################
usage() {
  echo "USAGE:   $0 ESP/serial/port"
  echo "Example: $0 /dev/ttyUSB0"
  quitHelp
  echo "This scipt helps simplify the enrollment process."
  echo "It functions as a serial monitor."
  echo "It can also automatically sign the CSR with local certificate autority."
  echo "This CA is provided via minimal OpenSSL CA with selfsigned certificate."
}

quitHelp() {
  echo "Press Ctrl+D to quit."
}

# Checks if current time > $1, if so prints connection help
printConnectionHelpIfNeeded() {
  local currentTime=$(date +%s)
  if [ "$READ_TIMEOUT" != "" ] && [ "$currentTime" -gt "$1" ]; then
    echo ""
    echo "Expected message from device not received"
    echo "Please note, that if a real PUF is used, it might take up to 30 secodns for the enrollment to begin"
    echo "1) Check that device is connected to the correct interface [$ESP_PATH]"
    echo "2) Press and hold the enrollment button and reset device"
    echo "====================================================================="
    READ_TIMEOUT="" # timeout no longer equired
  fi
}

######################################################################################################################
# CONFIG - you can modify default values
######################################################################################################################

# File where CSR is temporarly saved
CSR_FILE='generatedCSR.pem'

# File where certificate is temporary saved
CERT_FILE='generatedCERT.pem'

# CA certificate chain
CA_CERT_FILE='CA/certs/tyfamateCA_cert.pem'

# How to sign with local CA
SIGN_CUSTOM_CA() {
  openssl ca -config CA/openssl.cnf -batch -days 3650 -notext -md sha256 -key "CApassword" -in "$CSR_FILE" -out "$CERT_FILE"
  return $?
}

# true => prints script debug messages
DEBUG_SCRIPT=false

# true => prints debug messages from ESP
DEBUG_ESP=false

######################################################################################################################
# SERIAL - modify only if you know what you are doing
######################################################################################################################
BAUD=115200
STTY_SETTINGS="-brkint -icrnl -imaxbel -opost -onlcr -isig -icanon -iexten -echo -echoe -echok -echoctl -echoke"

######################################################################################################################
# RUN - modify only if you know what you are doing
######################################################################################################################

CSR="" #Contains CSR from ESP
ESP_PATH="" #Contains PATH to communicate with ESP

SCRIPT_SIGNED=false # true if CSR was signed by this script

CERTIFICATE_CHAIN="" #Contains signed certificate chain, from subject -> root
CERTIFICATE_CHAIN_COUNT=0 #Contains count of certificates in CERTIFICATE_CHAIN

READ_TIMEOUT="-t10" # intitial timeout settings for read -> valid until enrollment starts

debug() {
  if [ "$DEBUG_SCRIPT" = true ]; then
    echo "$@" 1>&2
  fi
}

validateCertificateChain() {
  CERTIFICATE_CHAIN_COUNT=$(echo -e "$CERTIFICATE_CHAIN" | awk '/-----BEGIN CERTIFICATE-----/ {beginCount += 1} /-----END CERTIFICATE-----/ {endCount += 1} END {if (beginCount==endCount) {print beginCount} else exit 1}')
  if [ "$?" -ne 0 ]; then
    echo "Mismatched TAG in certificate chain" 1>&2
    exit 5
  fi

  if [ "$CERTIFICATE_CHAIN_COUNT" -lt 1 ]; then
    echo "At least one certificate expected" 1>&2
    exit 6
  fi
}

externalSign() {
  debug "External CSR sign"
  local line=""
  local count=0

  echo "Please sign this CSR:"
  echo -e "$CSR"
  read -u 0 -p "Please press enter to continue..." line

  while IFS=$'\n' read -u 0 -p "How many certificates are in the chain? " count; do
    case "$count" in
      ''|*[!0-9]*)
        echo "Please only input numbers."
        continue
        ;;
    esac

    if [ "$count" -gt 0 ]; then
      break;
    else
      echo "At least one certificate is required."
    fi
  done

  echo "Insert certificate chain (PEM encoded). Starting with subject and ending with root certificate: "

  while [ "$count" -gt 0 ]; do
    read -u 0 line
    CERTIFICATE_CHAIN="$CERTIFICATE_CHAIN\n$line"
    if [ "$line" = "-----END CERTIFICATE-----" ]; then
      count=$((count-1))
    fi
  done
}

scriptSign() {
  debug "Scripted CSR sign"
  echo -e "$CSR" > "$CSR_FILE"
  SCRIPT_SIGNED=true
  SIGN_CUSTOM_CA >"/dev/null" 2>&1
  if [ $? -ne 0 ]; then
    echo "Unable to sign CSR" 1>&2
    exit 4
  fi
  CERTIFICATE_CHAIN="$(<$CERT_FILE)\n$(<$CA_CERT_FILE)"
}

signCSR() {
  debug "signing CSR"

  local yn=""
  while IFS=$'\n' read -u 0 -p "Would you like to automatically sign the CSR with local CA [Y/N]? " yn; do
    case $yn in
      [yY]* )
        scriptSign
        break;;
      [nN]* )
        externalSign
        break;;
      * )
        echo "Please answer [Y] or [N]";;
    esac
  done

  validateCertificateChain
}

sendCertificateChain() {
  debug "sending $CERTIFICATE_CHAIN_COUNT certificates"
  echo "$CERTIFICATE_CHAIN_COUNT" >&3
  echo -e "$CERTIFICATE_CHAIN" >&3
}

readESP() {
  debug "Reading started"

  exec 3<> "$ESP_PATH" # ESP connected over FD 3
  local state="E_INIT"
  local printHelpTime=$(date +%s)
  printHelpTime=$((printHelpTime+25)) # Wait 30 seconds for start of enrollment, if not print error

  local line=""
  while IFS=$'\n' read $READ_TIMEOUT -u 3 -r line || true; do
    firstChar=$(printf '%d' "'${line:0:1}") # Convert first char to its ASCII value
    if [ "$firstChar" -eq 27 ] && [ "${line:1:1}" = "[" ]; then # To support the use of this sciprt even with ESP debugging - prints the debug info, then skips
      if [ "$DEBUG_ESP" = true ]; then
        echo "ESP DEBUG: ${line:7}" 1>&2
      fi
    elif [ "$line" = "-----BEGIN ENROLLMENT-----" ]; then
      debug "Starting enrollment"
      state="E_FILL_CSR"
      READ_TIMEOUT="" # timeout no longer required
    elif [ "$line" = "-----BEGIN CERTIFICATE REQUEST-----" ]; then
      debug "Getting CSR"
      CSR="-----BEGIN CERTIFICATE REQUEST-----";
      state="E_RECEIVE_CSR"
    elif [ "$line" = "-----END CERTIFICATE REQUEST-----" ]; then
      debug "CSR recieved"
      CSR="${CSR}\n-----END CERTIFICATE REQUEST-----"
      state="E_WAIT"
    elif [ "$line" = "-----INPUT CERTIFICATE COUNT-----" ]; then
      debug "CERT count prompt received, can start signing"
      signCSR
      sendCertificateChain
      state="E_WAIT"
    elif [ "$line" = "-----END ENROLLMENT-----" ]; then
      break
    elif [ "$line" = "-----FAIL ENROLLMENT-----" ]; then
      echo "=================================="
      echo "The enrollment failed on ESP side."
      echo "Please retry."
      read -u 3 -r line
      echo "Reason provided by ESP: $line"
      exit 3
    else
      case "$state" in
        "E_INIT") #waiting for start of enrollment process, print help after delay
          debug "Waiting for enrollment"
          printConnectionHelpIfNeeded $printHelpTime
          ;;
        "E_FILL_CSR") # Filling CSR
          # TODO match headers with user friendly prompts
          debug "Filling CSR field"
          echo "$line"
          read -u 0 -r line
          debug "sending from user to ESP $line"
          echo "$line" >&3
          ;;
        "E_RECEIVE_CSR") # Receinve CSR
          debug "Getting next line of CSR"
          CSR="${CSR}\n$line"
          ;;
        "E_WAIT")
          debug "waiting"
          ;;
        *)
          exit 2
          ;;
      esac
    fi
  done
}


# check if port was supplied
[ $# -ne 1 ] && usage && exit 1
ESP_PATH="$1"

# Exit when any command fails -- we would be unable to fix it anyway
# set -e

# Backup stty settings of port -- restore later -- so we wont leave our settings behind
ORIGINAL_STTY_PORT="$(stty -g -F $ESP_PATH)"

# setup port settings
stty -F "$ESP_PATH" sane
stty -F "$ESP_PATH" $BAUD $STTY_SETTINGS

# On exit reset all stty settings and kill background reading process
trap 'set +e; unset IFS; exec 3>&-; stty -F "$ESP_PATH" "$ORIGINAL_STTY_PORT"' EXIT

#Print info
echo "Attempting to connect to ESP on $ESP_PATH."
quitHelp
echo "Please connect the device, press and hold the enrollment button and reset the device"

# Handle communciation
readESP

echo "==================================="
echo "The enrollment process is finished."
echo "The device should be booting up."
echo "You can now disconect the device."
exit 0
