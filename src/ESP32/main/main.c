#include <sys/cdefs.h>
/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-20
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf),
 * with the help of official documentation [1], ESP-IDF template project [2] and ESP-IDF example projects [3].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://github.com/espressif/esp-idf-template
 * [3] https://github.com/espressif/esp-idf/tree/master/examples
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#include "certificateCommunication.h"
#include "certificateHandler.h"
#include "globalWakeupState.h"
#include "accessPoint.h"
#include "simpleDNS.h"
#include "webserver_WolfSSL.h"
#include "tlsAwaitConnect.h"
#ifdef CONFIG_USE_MOCK_PUF
    #include "puflibMock.h"
#else
    #include "puflib.h"
#endif

#include <nvs_flash.h>
#include <esp_netif.h>
#include <esp_event.h>
#include <esp_sleep.h>
#include <esp_log.h>
#include <esp_check.h>
#include <driver/gpio.h>

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/wolfcrypt/logging.h>
#include <wolfssl/ssl.h>

#define BUTTON_PIN CONFIG_BUTTON_GPIO_NUMBER
#define LED_PIN CONFIG_LED_GPIO_NUMBER
#define STACK_DEPTH 2048

/**
 * @brief Delay between LED change
 */
static const TickType_t DELAY_LED = 500 / portTICK_PERIOD_MS;

/**
 * @brief Port on which server will listen
 */
#define HTTPS_PORT 443

/**
 * @brief Maximal number of concurrent connections
 */
#define MAX_CONCURRENT_CONNECTIONS 10

/**
 * @brief Length of UART buffer
 */
#define UART_BUFFER_SIZE 256

static const char *TAG = "app_main";

enum EWakeupTargets RTC_DATA_ATTR g_WakeupTarget;

/**
 * @brief Handles enrollment process
 */
static void handleEnrollment() {
    ESP_LOGI(TAG, "Starting enrollment procedure");

    SUART_BUFFER buffer = {
            .m_buffer = (char * const)malloc(UART_BUFFER_SIZE),
            .m_size = UART_BUFFER_SIZE,
            .m_used = 0,
    };

    bool ok = true;
    if (ok && !buffer.m_buffer) {
        ESP_LOGE(TAG, "Unable to allocate space for UART buffer");
        ok = false;
    }

    // Skip if resuming
    if (ok && !(g_WakeupTarget == ENROLL || g_WakeupTarget == VALIDATE) && !setupPUF()) {
        ESP_LOGE(TAG, "Unable to setup PUF");
        ok = false;
    }

    // Skip if resuming validate
    if (ok && g_WakeupTarget != VALIDATE && !createCSR(&buffer)) {
        ESP_LOGE(TAG, "Unable to create and send CSR");
        ok = false;
    }

    if (ok && !receiveCertificates(&buffer)) {
        ESP_LOGE(TAG, "Unable to receive certificates");
        ok = false;
    }

    free((void *)buffer.m_buffer);
}

/**
 * @brief Checks if enrollment process should be started and starts it
 */
static void checkForEnrollment() {
    ESP_LOGV(TAG, "Checking for enrollment procedure");
    gpio_set_direction(BUTTON_PIN, GPIO_MODE_INPUT);
    gpio_set_pull_mode(BUTTON_PIN, GPIO_PULLDOWN_ONLY);


    // Only when requested or reenter when deepsleep occurred
    if (gpio_get_level(BUTTON_PIN) || g_WakeupTarget == ENROLL || g_WakeupTarget == VALIDATE) { // Should enroll?
        ESP_LOGI(TAG, "Enrolment requested");
        if (configureUART()) { // Setup UART for enrollment
            ESP_LOGI(TAG, "UART configured");

            handleEnrollment();

            if (freeUART()) { // UART no longer needed
                ESP_LOGI(TAG, "UART freed");
            } else {
                ESP_LOGE(TAG, "Unable to free UART");
            }
        } else {
            ESP_LOGE(TAG, "Unable to configure UART");
        }
    } else {
        ESP_LOGI(TAG, "Enrolment not requested");
    }

    gpio_reset_pin(BUTTON_PIN);
}

/**
 * @brief Infinitely flashes led
 */
_Noreturn static void flashLED() {
    ESP_LOGI(TAG, "Flashing LED");
    gpio_set_direction(LED_PIN, GPIO_MODE_OUTPUT);

    while (true) {
        gpio_set_level(LED_PIN, 1);
        vTaskDelay(DELAY_LED);
        gpio_set_level(LED_PIN, 0);
        vTaskDelay(DELAY_LED);
    }
}

/**
 * @brief Checks if a certificate chain is present in memory
 * @return TRUE if present
 */
static bool isCertChainPresent() {
    ESP_LOGD(TAG, "Checking for presence of cert file");

    ESP_RETURN_ON_FALSE(initCertificateStorage(), false, TAG, "Unable to init cert storage");
    FILE *file = fopen(getCertificateFilename(), "r");
    fclose(file);
    ESP_RETURN_ON_FALSE(freeCertificateStorage(), false, TAG, "Unable to free cert storage");

    return file != NULL;
}

/**
 * @brief Calls puflib's wake up stub after waking up from deep sleep
 */
void RTC_IRAM_ATTR esp_wake_deep_sleep() {
    esp_default_wake_deep_sleep();

    // Call wakeup stub to initialize PUF
    puflib_wake_up_stub();
}

/**
 * @brief Sets up esp - nvs, netif, event loop...
 */
static void espInit() {
    ESP_LOGI(TAG, "App starting");
    //Initialize NVS
    esp_err_t NVSStatus = nvs_flash_init();
    if (NVSStatus == ESP_ERR_NVS_NO_FREE_PAGES || NVSStatus == ESP_ERR_NVS_NEW_VERSION_FOUND) {
        ESP_LOGW(TAG, "NVS flash init failed, attempting to erase");
        ESP_ERROR_CHECK(nvs_flash_erase());
        NVSStatus = nvs_flash_init();
    }
    ESP_ERROR_CHECK(NVSStatus);

    ESP_ERROR_CHECK(esp_netif_init());
    ESP_LOGV(TAG, "netif init");

    ESP_ERROR_CHECK(esp_event_loop_create_default());
    ESP_LOGV(TAG, "event loop created");

    int resetReason = esp_reset_reason(); // If not waking up then init wakeup target
    if (resetReason != ESP_RST_DEEPSLEEP && resetReason != ESP_RST_TASK_WDT) {
        ESP_LOGD(TAG, "Initializing g_WakeupTarget");
        g_WakeupTarget = NONE;
    }
}

void app_main() {
    puflib_init();
    ESP_LOGV(TAG, "PUF lib initialized");

    espInit();

    ESP_LOGI(TAG, "Wakeup target = %d", g_WakeupTarget);

    ESP_RETURN_ON_FALSE(wolfSSL_Init() == SSL_SUCCESS, , TAG, "Unable to initialize WolfSSL");
    ESP_LOGV(TAG, "WolfSSL initialized");

    if (!wolfSSL_Debugging_ON())
        ESP_LOGD(TAG, "WolfSSL debugging enabled");

    checkForEnrollment();

    if (!isCertChainPresent()) { // No need to start the device, TLS would fail
        ESP_LOGW(TAG, "No certificate present, stopping the device");
        flashLED();
    }

    accessPointInit();
    ESP_LOGV(TAG, "access point started");

    if (xTaskCreate(&simpleDNSRun, "mock DNS", STACK_DEPTH, NULL, tskIDLE_PRIORITY, NULL) != pdPASS) {
        ESP_LOGW(TAG, "Unable to start mock DNS, starting without");
    }

    ESP_LOGI(TAG, "using WolfSSL");
    g_WakeupTarget = NONE;
    tlsAwaitConnect(HTTPS_PORT, MAX_CONCURRENT_CONNECTIONS, handleHTTPSCommunication);
}
