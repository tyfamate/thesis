/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-04-22
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf),
 * with the help of official documentation [1], ESP-IDF template project [2] and ESP-IDF example projects [3].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://github.com/espressif/esp-idf-template
 * [3] https://github.com/espressif/esp-idf/tree/master/examples
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#ifndef PUF_IN_TLS_ON_ESP32_GLOBALWAKEUPSTATE_H
#define PUF_IN_TLS_ON_ESP32_GLOBALWAKEUPSTATE_H

enum EWakeupTargets {
    NONE, ENROLL, VALIDATE, WEBSERVER_INIT
};

extern enum EWakeupTargets g_WakeupTarget;

#endif //PUF_IN_TLS_ON_ESP32_GLOBALWAKEUPSTATE_H
