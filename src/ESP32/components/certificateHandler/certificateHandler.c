/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-03-08
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf) and wolfSSL (embedded TLS library).
 * With the help of official esp-idf [1] and wolfSSL [2] documentation, wolfSSL source code [3] and wolfSSL examples [4].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://www.wolfssl.com/docs/wolfssl-manual/ and https://www.wolfssl.com/doxygen/
 * [3] https://github.com/wolfSSL/wolfssl
 * [4] https://github.com/wolfSSL/wolfssl-examples
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#include "certificateHandler.h"
#include "globalWakeupState.h"
#include "accessPoint.h"
#ifdef CONFIG_USE_MOCK_PUF
    #include "puflibMock.h"
#else
    #include "puflib.h"
#endif

#include <stdint.h>
#include <stdbool.h>

#include <esp_log.h>
#include <esp_check.h>
#include <esp_spiffs.h>

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/wolfcrypt/ecc.h>
#include <wolfssl/wolfcrypt/asn_public.h>

static const char *TAG = "certificateHandler";

/**
 * @brief Certificate storage partition name
 */
#define PARTITION "/spiffs"

/**
 * @brief Selected curve name
 */
#define CURVE_NAME CONFIG_CURVE_NAME

/**
 * @brief Max size of DER encoded private+public key
 */
#define KEY_DER_SIZE 1024

/**
 * @brief Storage for DER encoded private+public key
 */
static uint8_t g_KEY_DER[KEY_DER_SIZE];

/**
 * @brief Gets curve id from configured curve name
 * @return curve id of selected curve
 */
static ecc_curve_id getCurveID() {
    int returnVal = wc_ecc_get_curve_id_from_name(CURVE_NAME);
    if (returnVal < 0) {
        ESP_LOGW(TAG, "Unable to get curve ID from curve name");
    }
    return returnVal;
}

/**
 * @brief Fills private part of ecc_key with key from PUF
 * @param key [in/out] key structure to be filled out
 * @return TRUE on success
 */
static bool getKeyStructurePrivate(ecc_key *key) {
    if (!key)
        return false;
    int keyLength = wc_ecc_get_curve_size_from_id(getCurveID());
    if (keyLength < 0) {
        ESP_LOGE(TAG, "Unable to determine key length from curve id");
        return false;
    }

    if (PUF_STATE != RESPONSE_READY && !get_puf_response()) {
        ESP_LOGI(TAG, "PUF not ready, deepsleep required with target %d", g_WakeupTarget);
        get_puf_response_reset();
    }

    if (PUF_STATE != RESPONSE_READY) {
        // Should never get here, puflib indicated that puf is available, but it is not
        ESP_LOGE(TAG, "PUF indicated that response should be available but state is clean");
        return false;
    }

    if (keyLength > PUF_RESPONSE_LEN) {
        clean_puf_response();
        ESP_LOGE(TAG, "PUF response is not long enough as a key for selected curve");
        return false;
    }

    // Import private key only
    int returnVal = wc_ecc_import_private_key_ex(PUF_RESPONSE, keyLength, NULL, 0, key, getCurveID());
    clean_puf_response();
    return returnVal == 0;
}

/**
 * @brief If PUF_DEBUG_SECRET is defined prints secret key in pem format, otherwise nothing
 * @param key [in] key to be printed
 */
static inline void DEBUG_printSecretKey(const ecc_key * const key) {
#ifdef PUF_DEBUG_SECRET
    #define DER_BUFFER_SIZE 1024
    #define PEM_BUFFER_SIZE (DER_BUFFER_SIZE*2)

    if (!key)
        return;

    uint8_t *der = (uint8_t *)malloc(DER_BUFFER_SIZE);
    uint8_t *pem = (uint8_t *)malloc(PEM_BUFFER_SIZE);
    if (!der || !pem)  {
        ESP_LOGW(TAG, "Unable to allocate key der+pem buffer");
        free(der);
        free(pem);
        return;
    }

    int derLength = wc_EccKeyToDer(key, der, DER_BUFFER_SIZE);
    if (derLength <= 0) {
        ESP_LOGW(TAG, "Unable to export key as DER");
        free(der);
        free(pem);
        return;
    }

    int pemLength = wc_DerToPem(der, derLength, pem, PEM_BUFFER_SIZE - 1, ECC_PRIVATEKEY_TYPE);
    if (pemLength <= 0) {
        ESP_LOGW(TAG, "Unable to transform to PEM [%d]", pemLength);
        free(der);
        free(pem);
        return;
    }
    pem[pemLength] = '\0';
    printf("\n\nPEM:\n%s\n(%d)\n", pem, pemLength);
    free(der);
    free(pem);
#endif
}

//======================================================================================================================
// PUBLIC FACING FUNCTIONS
//======================================================================================================================
bool setupPUF() {
    ESP_LOGI(TAG, "Starting a PUF provisioning");
    provision_puf();
    ESP_LOGV(TAG, "PUF provisioned");
    return true;
}

const uint8_t *getKeyDER(long *length) {
    if (!length)
        return NULL;

    ecc_key key;
    if (wc_ecc_init(&key) != 0) {
        ESP_LOGE(TAG, "Unable to initialise ECC key");
        return NULL;
    }
    ESP_LOGV(TAG, "ECC key initialised");

    if (!getKeyStructure(&key)) {
        ESP_LOGE(TAG, "Unable to get key structure");
        wc_ecc_free(&key);
        return NULL;
    }

    int derLength = wc_EccKeyToDer(&key, g_KEY_DER, KEY_DER_SIZE);
    wc_ecc_free(&key);
    if (derLength <= 0) {
        ESP_LOGE(TAG, "Unable to export key as DER [%d]", derLength);
        return NULL;
    }
    ESP_LOGV(TAG, "Key exported as DER, size %d", derLength);
    *length = derLength;

    return g_KEY_DER;
}

void freeKeyDER() {
    memset(g_KEY_DER, 0, KEY_DER_SIZE);
}

bool getKeyStructure(ecc_key *key) {
    if (!key)
        return false;

    if (!getKeyStructurePrivate(key)) {
        ESP_LOGE(TAG, "Unable to fill private part of key structure");
        return false;
    }
    ESP_LOGV(TAG, "ECC private key imported");

    // Calculate public key from private and cache it in "key"
    if (wc_ecc_make_pub(key, NULL) != MP_OKAY) {
        ESP_LOGE(TAG, "ECC public key cannot be calculated");
        return false;
    }

    DEBUG_printSecretKey(key);
    ESP_LOGV(TAG, "ECC private key imported");

    if (wc_ecc_check_key(key) != MP_OKAY) {
        ESP_LOGE(TAG, "ECC key is not valid");
        return false;
    }
    ESP_LOGV(TAG, "ECC key checked");

    return true;
}

bool initCertificateStorage() {
    esp_vfs_spiffs_conf_t conf = {.base_path = PARTITION, .partition_label = NULL, .max_files = 1, .format_if_mount_failed = true};

    ESP_RETURN_ON_FALSE(esp_vfs_spiffs_register(&conf) == ESP_OK, false, TAG, "Unable initialize SPIFFS partition");
    ESP_LOGD(TAG, "File system initialized");
    return true;
}

bool freeCertificateStorage() {
    ESP_RETURN_ON_FALSE(esp_vfs_spiffs_unregister(NULL) == ESP_OK, false, TAG, "Unable free SPIFFS partition");
    ESP_LOGD(TAG, "File system freed");
    return true;
}

const char *getCertificateFilename() {
    return PARTITION"/cert.pem";
}