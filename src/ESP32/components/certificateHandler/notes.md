### Parse der key to ASN1

`openssl asn1parse -inform DER -in key.der -i -dump`


### OpenSSL create cert + CSR + sign

`openssl req -new -sha256 -keyform der -key key.der -out csr.csr`

`openssl req -x509 -sha256 -days 3650 -keyform der -key key.der -in csr.csr -outform der -out certificate.der`

## EC private key

https://8gwifi.org/PemParserFunctions.jsp

Parsed to ANS1:

```                                                                                           ✔
0:d=0  hl=2 l= 119 cons: SEQUENCE          
2:d=1  hl=2 l=   1 prim:  INTEGER           :01
5:d=1  hl=2 l=  32 prim:  OCTET STRING      
0000 - d3 df bc 45 5b c1 22 a6-ea 31 a9 17 46 44 91 1b   ...E[."..1..FD..
0010 - ff 33 89 de c7 60 3c 41-85 52 d0 c1 39 e7 e7 ae   .3...`<A.R..9...
39:d=1  hl=2 l=  10 cons:  cont [ 0 ]        
41:d=2  hl=2 l=   8 prim:   OBJECT            :prime256v1
51:d=1  hl=2 l=  68 cons:  cont [ 1 ]        
53:d=2  hl=2 l=  66 prim:   BIT STRING        
0000 - 00 04 81 78 98 54 1e a0-35 77 ac a0 47 6a b0 55   ...x.T..5w..Gj.U
0010 - 5e 75 e0 2d e7 c3 cd f3-d4 f3 15 b2 9d 9c 88 69   ^u.-...........i
0020 - 55 10 fc a1 40 15 27 a1-7d ff 1e 16 48 15 dc 14   U...@.'.}...H...
0030 - 3b 5e 8b 1a fd 7a f4 da-00 c2 6c 0d b5 1b 7b f7   ;^...z....l...{.
0040 - f1 a2                                             ..
```

5: - private key

256b

https://www.baeldung.com/linux/openssl-extract-certificate-info

https://cryptobook.nakov.com/asymmetric-key-ciphers/elliptic-curve-cryptography-ecc

https://decoder.link/matcher

https://8gwifi.org/PemParserFunctions.jsp


# WolfSSL CSR used fields
```
keyType = ecc key
bodySz = der body size
version
subject = must not be empty - or XSTRLEN on CSR->sbjRaw uses RAW subject
    sur - surname does not make sense to support without given / initials / pseudonym
isCA = sets basic constrains
altNamesSz (if WOLFSSL_ALT_NAMES)
    uses altNames
challengePw + challengePwPrintableString

(if WOLFSSL_CERT_EXT)
skidSz > 0
    uses skid
keyUsage != 0
    uses keyUsage
extKeyUsage != 0
    uses extKeyUsage

(if WOLFSSL_CUSTOM_OID)
custom OID via SetCustomObjectID
    cert->extCustom
```