/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-03-14
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf) and wolfSSL (embedded TLS library).
 * With the help of official esp-idf [1] and wolfSSL [2] documentation, wolfSSL source code [3] and wolfSSL examples [4].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://www.wolfssl.com/docs/wolfssl-manual/ and https://www.wolfssl.com/doxygen/
 * [3] https://github.com/wolfSSL/wolfssl
 * [4] https://github.com/wolfSSL/wolfssl-examples
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#include "certificateHandler.h"
#include "certificateCommunication.h"
#include "certificateNVS.h"
#include "globalWakeupState.h"

#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <math.h>

#include <esp_log.h>
#include <esp_check.h>
#include <driver/uart.h>
#include <esp_spiffs.h>
#include <nvs_flash.h>

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/wolfcrypt/logging.h>
#include <wolfssl/wolfcrypt/ecc.h>
#include <wolfssl/wolfcrypt/asn_public.h>
#include <wolfssl/ssl.h>

// DER+PEM buffer defines
/**
 * @brief Size of buffer where DER representation is saved
 */
#define DER_BUFFER_SIZE 512

// In order to fit whole DER buffer it is required to have space for BASE64 encoding: 4 * ceil(n / 3)
#define PEM_BUFFER_SIZE_BASE64_RAW (4 * (int)(ceil(DER_BUFFER_SIZE / 3.0)))
// PLUS space for header + footer 2x(<40 chars)
#define PEM_BUFFER_SIZE_HEADER (2 * 40)
// PLUS every 64 chars in encoded output there is a '\n'
#define PEM_BUFFER_SIZE_NEWLINES ((int)(ceil(PEM_BUFFER_SIZE_BASE64_RAW / 64.0)))
// So PEM should now fit even a full DER buffer

/**
 * @brief Size of buffer where PEM is saved
 */
#define PEM_BUFFER_SIZE (PEM_BUFFER_SIZE_HEADER + PEM_BUFFER_SIZE_BASE64_RAW + PEM_BUFFER_SIZE_NEWLINES)

/**
 * @brief Size of buffer where certificate count is read
 */
#define CERTIFICATE_COUNT_BUFFER_SIZE 16

// UART DEFINES
/**
 * @brief UART port identifier
 */
#define UART_PORT UART_NUM_0

/**
 * @brief Size of unified UART buffer
 */
#define UART_BUFFER_SIZE 512

/**
 * @brief Poling rate for UART
 */
#define UART_POLING_RATE (500 / portTICK_PERIOD_MS)

/**
 * @brief Maximum time for which single UART flush will be attempted
 */
#define UART_FLUSH_TIMEOUT (20000 / portTICK_PERIOD_MS)

/**
 * @brief Contains a DER encoded CSR
 */
static RTC_DATA_ATTR uint8_t g_DERBuffer[DER_BUFFER_SIZE];

/**
 * @brief Contains length of data inside g_DERBuffer
 */
static RTC_DATA_ATTR int g_DERBufferLength;

/**
 * @brief Contains CSR before signing
 */
static RTC_DATA_ATTR Cert g_CSR;

/**
 * @brief stores hostname between CSR creation and NVS commit
 */
static RTC_DATA_ATTR char g_HostName[HOST_NAME_MAX_LENGTH];

//=============================================
// Prompts and messages for UART communication
//=============================================
#define PROMPT_CERT_AMOUNT "-----INPUT CERTIFICATE COUNT-----"
#define PROMPT_CERT "-----INPUT CERTIFICATE CHAIN-----"

#define PROMPT_ENROLMENT_BEGIN "-----BEGIN ENROLLMENT-----"
#define PROMPT_ENROLMENT_END "-----END ENROLLMENT-----"
#define PROMPT_ENROLMENT_FAIL "-----FAIL ENROLLMENT-----"

#define PROMPT_CSR_COMMONNAME "-----INPUT COMMONNAME-----"
#define PROMPT_CSR_SERIAL "-----INPUT SERIALNUMBER-----" // Printable string
#define PROMPT_CSR_COUNTRY "-----INPUT C-----" // Printable string 2 + ISO 3166 alpha 2 codes only
#define PROMPT_CSR_LOCALITY  "-----INPUT L-----"
#define PROMPT_CSR_STATE "-----INPUT ST-----"
#define PROMPT_CSR_STREET "-----INPUT STREET-----"
#define PROMPT_CSR_ORG "-----INPUT O-----"
#define PROMPT_CSR_UNIT "-----INPUT OU-----"
#define PROMPT_CSR_POSTALCODE "-----INPUT POSTALCODE-----"
#define PROMPT_CSR_EMAIL "-----INPUT MAIL-----" // IA5String
#define PROMPT_CSR_CHALLENGE "-----INPUT CACHALLENGE-----"

static const char *TAG = "certificateCommunication";

/**
 * @brief Copies line from uart buffer to target, and shifts buffer
 * @param uart [in/out] uart helper buffer
 * @param lineLength [in] length of line to be copied
 * @param target [out] target to where wo write
 * @param targetMaxLength [in] maximal size of target
 * @return TRUE on success
 */
static bool moveLineAndShift(SUART_BUFFER *uart, int lineLength, char *target, int targetMaxLength) {
    if (!uart)
        return false;

    if (lineLength + 1 > targetMaxLength) {
        ESP_LOGE(TAG, "Unable to fit line in target buffer");
        return false;
    }

    memcpy(target, uart->m_buffer, lineLength);
    uart->m_buffer[lineLength] = '\0';

    memcpy(uart->m_buffer, uart->m_buffer + lineLength + 1, uart->m_used - lineLength - 1);
    uart->m_used = uart->m_used - lineLength - 1;
    ESP_LOGV(TAG, "Value: %s", target);
    return true;
}

/**
 * @brief Checks weather or not target contains only characters from the IA5String (ITU-T Rec. X.680) character set
 * @param target null terminated buffer to be checked
 * @return TRUE if target is IA5String, FALSE otherwise or if target==NULL
 */
static bool isIA5String(const char *const target) {
    if (!target)
        return false;
    for (size_t i = 0; target[i] != '\0'; ++i) {
        if (target[i] & ~0x7f)
            return false;
    }
    return true;
}

/**
 * @brief Reads and saves one line from UART to target
 * @param uart [in/out] uart helper buffer
 * @param target [out] target to where wo write
 * @param targetMaxLength [in] maximal size of target (including null terminator)
 * @return TRUE on success
 */
static bool getLine(SUART_BUFFER *uart, char *target, int targetMaxLength) {
    if (!target || !uart)
        return false;
    ESP_LOGV(TAG, "Getting line");
    int lineLength = 0;
    do { // Check if another line is not already in buffer, then get new part
        for ( ; lineLength < uart->m_used; ++lineLength) {
            if (uart->m_buffer[lineLength] == '\r' || uart->m_buffer[lineLength] == '\n') { // Newline has broken encoding in ESP-IDF monitor
                ESP_LOGV(TAG, "Found line, copying");
                return moveLineAndShift(uart, lineLength, target, targetMaxLength);
            }
        }

        if (lineLength >= targetMaxLength) // Impossible to fit to target
            return false;

        if (uart->m_used >= uart->m_size) { // Buffer full - offload it to output
            memcpy(target, uart->m_buffer, uart->m_used);
            target = target + uart->m_used;
            targetMaxLength = targetMaxLength - uart->m_used;
            lineLength = 0;
            uart->m_used = 0;
        }

        int readLength;
        do {
            readLength = uart_read_bytes(UART_PORT, uart->m_buffer + uart->m_used, uart->m_size - uart->m_used, UART_POLING_RATE);
        } while (readLength == 0);
        uart->m_used += readLength;
    } while (lineLength < targetMaxLength);

    return false;
}

/**
 * @brief Sends UART message and flushes
 * @param message [in] message to be send
 */
static void flushMessage(const char * const message) {
    uart_write_bytes(UART_PORT, message, strlen(message));
    uart_write_bytes(UART_PORT, "\n", 1);
    esp_err_t txError;
    do {
        txError = uart_wait_tx_done(UART_PORT, UART_FLUSH_TIMEOUT);
    } while (txError == ESP_ERR_TIMEOUT);
}

/**
 * @brief Decides on required encoding of string and sets corresponding type
 * @param string [in] string (zero terminated) of which encoding to be checked
 * @param encoding [out] target for encoding choice
 */
static void setANS1Encoding(const char * const string, char *encoding) {
    if (!string || !encoding)
        return;
    // PrintableString - subset of ASCII
    // ITU-T Rec. X.680 (07/2002) - TABLE 8 - page 57
    for (size_t i = 0; string[i] != '\0'; ++i) {
        if (isalnum((int)string[i])) // A-Z + a-z + 0-9
            continue;
        switch (string[i]) {
            case ' ':
            case '\'':
            case '(':
            case ')':
            case '+':
            case ',':
            case '-':
            case '.':
            case '/':
            case ':':
            case '=':
            case '?':
                continue;
            default:
                break;
        }
        *encoding = CTC_UTF8;
        return;
    }

    *encoding = CTC_PRINTABLE;
}

/**
 * @brief Fils CSR based on input from UART
 * CSR is stored in g_CSR
 * @param uart [in/out] uart helper buffer
 * @return TRUE on success
 */
static bool fillCSR(SUART_BUFFER *uart) {
    if (!uart)
        return false;

    wc_InitCert(&g_CSR);
    ESP_LOGV(TAG, "CSR initialised");

    flushMessage(PROMPT_ENROLMENT_BEGIN);

    flushMessage(PROMPT_CSR_COMMONNAME);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.commonName, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with commonName");
    setANS1Encoding(g_CSR.subject.commonName, &g_CSR.subject.commonNameEnc);

    flushMessage(PROMPT_CSR_SERIAL);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.serialDev, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with serial");
    setANS1Encoding(g_CSR.subject.serialDev, &g_CSR.subject.serialDevEnc);
    ESP_RETURN_ON_FALSE(g_CSR.subject.serialDevEnc == CTC_PRINTABLE, false, TAG, "Serial contains a non valid character");

    flushMessage(PROMPT_CSR_COUNTRY);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.country, 3), false, TAG, "Unable to configure CSR with country");
    setANS1Encoding(g_CSR.subject.country, &g_CSR.subject.countryEnc);
    ESP_RETURN_ON_FALSE(g_CSR.subject.countryEnc == CTC_PRINTABLE && strlen(g_CSR.subject.country) != 1, false, TAG, "Country contains a non valid character"); // Allow empty or 2 length

    flushMessage(PROMPT_CSR_LOCALITY);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.locality, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with locality");
    setANS1Encoding(g_CSR.subject.locality, &g_CSR.subject.localityEnc);

    flushMessage(PROMPT_CSR_STATE);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.state, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with state");
    setANS1Encoding(g_CSR.subject.state, &g_CSR.subject.stateEnc);

    flushMessage(PROMPT_CSR_STREET);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.street, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with street");
    setANS1Encoding(g_CSR.subject.street, &g_CSR.subject.streetEnc);

    flushMessage(PROMPT_CSR_ORG);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.org, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with organization");
    setANS1Encoding(g_CSR.subject.org, &g_CSR.subject.orgEnc);

    flushMessage(PROMPT_CSR_UNIT);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.unit, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with unit");
    setANS1Encoding(g_CSR.subject.unit, &g_CSR.subject.unitEnc);

    //ifdef WOLFSSL_CERT_EXT CSR->subject.busCat ?= business category? Private Organization
        // joiC - joi country? - US
        // joiSt - joi state? - Oregon

    flushMessage(PROMPT_CSR_POSTALCODE);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.postalCode, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with postal code");
    setANS1Encoding(g_CSR.subject.postalCode, &g_CSR.subject.postalCodeEnc);

    flushMessage(PROMPT_CSR_EMAIL);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.subject.email, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with email");
    ESP_RETURN_ON_FALSE(isIA5String(g_CSR.subject.email), false, TAG, "Email contains invalid characters");

    flushMessage(PROMPT_CSR_CHALLENGE);
    ESP_RETURN_ON_FALSE(getLine(uart, g_CSR.challengePw, CTC_NAME_SIZE), false, TAG, "Unable to configure CSR with challenge");
    setANS1Encoding(g_CSR.challengePw, (char *)&g_CSR.challengePwPrintableString);
    g_CSR.challengePwPrintableString = (g_CSR.challengePwPrintableString == CTC_PRINTABLE) ? 1 : 0;

    ESP_LOGV(TAG, "CSR filled");
    return true;
}

/**
 * @brief Convert Cert CSR to DER and sign it
 * The CSR is from g_CSR, DER is saved inside g_DERBuffer
 * @return TRUE on success
 */
static bool signCSR() {
    ESP_LOGD(TAG, "Signing CSR");

    ecc_key key;
    int returnVal = wc_ecc_init(&key);
    if (returnVal != 0) {
        ESP_LOGW(TAG, "Unable to initialise ECC key [%d]", returnVal);
        return false;
    }
    ESP_LOGV(TAG, "ECC key initialised");

    enum EWakeupTargets oldWakeup = g_WakeupTarget; // Make a copy of old wakeup state and reset it if successful
    g_WakeupTarget = ENROLL;
    if (!getKeyStructure(&key)) {
        g_WakeupTarget = oldWakeup;
        ESP_LOGE(TAG, "Unable to get key structure");
        wc_ecc_free(&key);
        return false;
    }
    g_WakeupTarget = oldWakeup;

    ESP_LOGV(TAG, "Transforming CSR to DER");
    if (wc_MakeCertReq(&g_CSR, g_DERBuffer, DER_BUFFER_SIZE, NULL, &key) <= 0) {
        ESP_LOGE(TAG, "Unable to convert CSR to der");
        wc_ecc_free(&key);
        return false;
    }
    g_DERBufferLength = g_CSR.bodySz;

    WC_RNG rng;
    returnVal = wc_InitRng(&rng);
    if (returnVal != 0) {
        ESP_LOGW(TAG, "Unable to initialise RNG [%d]", returnVal);
        wc_ecc_free(&key);
        return false;
    }
    ESP_LOGV(TAG, "RNG initialised");

    g_DERBufferLength = wc_SignCert(g_DERBufferLength, CTC_SHA256wECDSA, g_DERBuffer, DER_BUFFER_SIZE, NULL, &key, &rng);
    wc_FreeRng(&rng);
    wc_ecc_free(&key);
    if (g_DERBufferLength <= 0) {
        ESP_LOGE(TAG, "Unable to sign CSR [%d]", g_DERBufferLength);
        return false;
    }

    ESP_LOGV(TAG, "CSR signed");
    return true;
}

/**
 * @brief Sends DER encoded CSR over UART
 * DER is from g_DERBuffer
 * @return TRUE on success
 */
static bool sendCSR() {
    uint8_t pem[PEM_BUFFER_SIZE];
    int pemLength = wc_DerToPem(g_DERBuffer, g_DERBufferLength, pem, sizeof(pem) - 1, CERTREQ_TYPE);
    if (pemLength <= 0) {
        ESP_LOGE(TAG, "Unable to transform to PEM [%d]", pemLength);
        return false;
    }
    ESP_LOGV(TAG, "Transformed to PEM");

    pem[pemLength] = '\0';
    flushMessage((char *)pem);
    return true;
}

/**
 * @brief Validates certificate chain stored in certificate file
 * @return TRUE on success
 */
static bool certificateChainValid() {
    ESP_LOGV(TAG, "Validating certificate chain");
    WOLFSSL_CTX *ctx = wolfSSL_CTX_new(wolfTLSv1_3_server_method());
    if (!ctx) {
        ESP_LOGE(TAG, "Unable to create CTX for certificate validation");
        return false;
    }

    long keySize;

    enum EWakeupTargets oldWakeup = g_WakeupTarget; // Make a copy of wakeup state to reset it later
    g_WakeupTarget = VALIDATE;
    const uint8_t *keyBuffer = getKeyDER(&keySize);
    g_WakeupTarget = oldWakeup;

    bool ok = true;
    if (ok && wolfSSL_CTX_use_PrivateKey_buffer(ctx, keyBuffer, keySize, SSL_FILETYPE_ASN1) != SSL_SUCCESS) {
        ESP_LOGE(TAG, "Unable to read private key");
        ok = false;
    }
    freeKeyDER();

    if (ok && wolfSSL_CTX_use_certificate_chain_file(ctx, getCertificateFilename()) != SSL_SUCCESS) {
        ESP_LOGE(TAG, "Unable to read certificate chain file");
        ok = false;
    }

    if (ok && wolfSSL_CTX_check_private_key(ctx) != SSL_SUCCESS) {
        ESP_LOGE(TAG, "Certificate does not match private key");
        ok = false;
    }
    wolfSSL_CTX_free(ctx);
    return ok;
}

/**
 * @brief Reads and saves certificate chain from UART to certificate file
 * @param uart [in/out] uart helper buffer
 * @param certificateCount [in] number of certificates in chain
 * @return TRUE on success
 */
static bool receiveCertificateChain(SUART_BUFFER *uart, unsigned long certificateCount) {
    if (!uart)
        return false;

    FILE *certificateFD = fopen(getCertificateFilename(), "w"); // Discard previous certificates
    ESP_RETURN_ON_FALSE(certificateFD, false, TAG, "Unable to open certificate file for writing");
    unsigned long dashCount = 0;
    do { // Read all certificates
        while (uart->m_used == 0) {
            uart->m_used = uart_read_bytes(UART_PORT, uart->m_buffer, uart->m_size, UART_POLING_RATE);
        }

        int toWrite = uart->m_used;
        for (int i = 0; i < uart->m_size; ++i) {
            if (uart->m_buffer[i] == '\r') { // Fix CR in buffer, ESP-IDF monitor tool uses \r for some reasons
                uart->m_buffer[i] = '\n';
            } else if (uart->m_buffer[i] == '-') {
                ++dashCount;
                if (dashCount >= certificateCount * 20) {
                    toWrite = i + 1; // Stop at last certificate
                    break;
                }
            }
        }

        size_t writenCnt = fwrite(uart->m_buffer, sizeof(char), toWrite, certificateFD);

        uart->m_used = uart->m_used - toWrite;
        memcpy(uart->m_buffer, uart->m_buffer + toWrite, uart->m_used);

        if (writenCnt != toWrite) {
            ESP_LOGE(TAG, "Unable to write to certificate file");
            fclose(certificateFD);
            return false;
        }
    } while (dashCount < certificateCount * 20);
    ESP_LOGD(TAG, "Size of certificate file: %lu", ftell(certificateFD));
    fwrite("\n", sizeof(char), 1, certificateFD); // POSIX \n at the end of file
    fclose(certificateFD);

    return true;
}

/**
 * @brief Reads and parses certificate count form UART
 * @param uart [in/out] uart helper buffer
 * @return count of certificates / 0 on error
 */
static unsigned int receiveCertificateCount(SUART_BUFFER *uart) {
    if (!uart)
        return false;

    char countBuffer[CERTIFICATE_COUNT_BUFFER_SIZE];

    if (!getLine(uart, countBuffer, CERTIFICATE_COUNT_BUFFER_SIZE)) {
        ESP_LOGE(TAG, "Unable to get line with certificate count");
        return 0;
    }

    char *countEndPtr = NULL;
    errno = 0;
    unsigned int certificateCount = strtoul(countBuffer, &countEndPtr, 10);
    if (errno != 0) {
        ESP_LOGE(TAG, "Unable to parse certificate count");
        return 0;
    }

    return certificateCount;
}

/**
 * @brief Saves hostname from CSR to NVS
 * @return TRUE on success
 */
static bool saveHostName() {
    nvs_handle_t handle;
    if (nvs_open(HOST_NAME_NVS_NAME, NVS_READWRITE, &handle) != ESP_OK) {
        ESP_LOGE(TAG, "Unable to open NVS to set hostname");
        return false;
    }

    if (nvs_set_str(handle, HOST_NAME_NVS_KEY, g_HostName) != ESP_OK) {
        ESP_LOGE(TAG, "Unable to set hostname in NVS");
        return false;
    }

    if (nvs_commit(handle)) {
        ESP_LOGE(TAG, "Unable to commit NVS changes");
        return false;
    }

    nvs_close(handle);
    return true;
}

/**
 * @brief Internal function that receives and validates signed certificates over serial for function "receiveCertificates"
 * It requires initialized certificate storage beforehand
 * @param buffer pointer to be used as UART buffer
 * @return TRUE on success
 */
static bool receiveCertificatesAndValidate(SUART_BUFFER *uart) {
    if (!uart)
        return false;

    // if resuming validation -- just attempt to validate
    if (g_WakeupTarget == VALIDATE) {
        g_WakeupTarget = NONE;
        ESP_LOGE(TAG, "Validation certificate chain with deepsleep key");
        return certificateChainValid();
    }

    flushMessage(PROMPT_CERT_AMOUNT);
    unsigned int certificateCount = receiveCertificateCount(uart);
    ESP_LOGV(TAG, "Certificate count: %u", certificateCount);

    bool ok = true;
    if (!certificateCount) {
        ESP_LOGE(TAG, "Unable to receive / 0 received certificate count");
        ok = false;
    }

    flushMessage(PROMPT_CERT);
    if (ok && !receiveCertificateChain(uart, certificateCount)) {
        ESP_LOGE(TAG, "Unable to receive certificate chain");
        ok = false;
    }

    if (ok && !certificateChainValid()) {
        ESP_LOGE(TAG, "Certificate chain is not valid");
        ok = false;
    }

    if (ok && !saveHostName())
        ESP_LOGW(TAG, "Unable to save hostname");

    return ok;
}


//======================================================================================================================
// PUBLIC FACING FUNCTIONS
//======================================================================================================================

bool configureUART() {
    uart_config_t uart_config = {
            .baud_rate = CONFIG_ESPTOOLPY_MONITOR_BAUD,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
            .source_clk = UART_SCLK_APB,
    };

    ESP_RETURN_ON_FALSE(uart_param_config(UART_PORT, &uart_config) == ESP_OK, false, TAG, "Unable to configure UART port");
    ESP_RETURN_ON_FALSE(uart_set_pin(UART_PORT, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE) == ESP_OK, false, TAG, "Unable to set UART pin");
    ESP_RETURN_ON_FALSE(uart_driver_install(UART_PORT, UART_BUFFER_SIZE * 2, 0, 0, NULL, 0) == ESP_OK, false, TAG, "Unable to install UART driver");
    return true;
}

bool freeUART() {
    return uart_driver_delete(UART_PORT) == ESP_OK;
}

bool createCSR(SUART_BUFFER *uart) {
    if (!uart)
        return false;

    ESP_LOGD(TAG, "Starting CSR process");
    bool ok = true;

    if (g_WakeupTarget == ENROLL) { // If resume from deepsleep PUF -> only sign
        g_WakeupTarget = NONE;
        if (ok && !signCSR(NULL)) {
            ESP_LOGE(TAG, "Unable to sing CSR with deepsleep key");
            ok = false;
        }
    } else { // Normal enrollment
        if (ok && !fillCSR(uart)) {
            ESP_LOGE(TAG, "Unable to fill CSR");
            ok = false;
        }

        if (ok && HOST_NAME_MAX_LENGTH > strlen(g_CSR.subject.commonName)) { // Set hostname aside for DNS
            ESP_LOGV(TAG, "Setting aside hostname for DNS");
            strcpy(g_HostName, g_CSR.subject.commonName);
        }

        if (ok && !signCSR()) {
            ESP_LOGE(TAG, "Unable to sing CSR");
            ok = false;
        }
    }

    if (ok && !sendCSR()) {
        ESP_LOGE(TAG, "Unable to send CSR");
        ok = false;
    }

    if (!ok)
        flushMessage(PROMPT_ENROLMENT_FAIL);

    return ok;
}

bool receiveCertificates(SUART_BUFFER *uart) {
    if (!uart)
        return false;

    bool ok = true;
    if (ok && !initCertificateStorage()) {
        ESP_LOGE(TAG, "Unable to initialize certificate storage");
        ok = false;
    }

    if (ok && !receiveCertificatesAndValidate(uart)) {
        ESP_LOGE(TAG, "Unable to receive and validate certificates");
        ok = false;
    }

    if (!freeCertificateStorage()) {
        ESP_LOGE(TAG, "Unable to free certificate storage");
        ok = false;
    }

    if (ok)
        flushMessage(PROMPT_ENROLMENT_END);
    else
        flushMessage(PROMPT_ENROLMENT_FAIL);

    return ok;
}