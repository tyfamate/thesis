/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-03-08
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf) and wolfSSL (embedded TLS library).
 * With the help of official esp-idf [1] and wolfSSL [2] documentation, wolfSSL source code [3] and wolfSSL examples [4].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://www.wolfssl.com/docs/wolfssl-manual/ and https://www.wolfssl.com/doxygen/
 * [3] https://github.com/wolfSSL/wolfssl
 * [4] https://github.com/wolfSSL/wolfssl-examples
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#ifndef PUF_IN_TLS_ON_ESP32_CERTIFICATEHANDLER_H
#define PUF_IN_TLS_ON_ESP32_CERTIFICATEHANDLER_H

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/wolfcrypt/ecc.h>
#include <stdint.h>
#include <stdbool.h>

/**
 * @brief Sets up all required parts to successfully use PUF on this device
 * @return TRUE on success
 */
bool setupPUF();

/**
 * @brief Return DER encoded private+public key for certificate provided by getCertificate
 * @param length [OUT] length of key, in bytes
 * @return pointer to DER representation of private key
 */
const uint8_t *getKeyDER(long *length);

/**
 * @brief Frees DER encoded private+public key provided by getKeyDER
 */
void freeKeyDER();

/**
 * @brief Fills initiated ecc_key structure with private key
 * @return TRUE on success
 */
bool getKeyStructure(ecc_key *);

/**
 * @brief Setups certificate storage
 * @return TRUE on success
 */
bool initCertificateStorage();

/**
 * @brief Frees filesystem used by certificate storage
 * @return TRUE on success
 */
bool freeCertificateStorage();

/**
 * @brief Return path to PEM encoded certificate chain to be used with key provided by getKeyDER
 * @return null-terminated path to certificate chain file
 */
const char *getCertificateFilename();

#endif //PUF_IN_TLS_ON_ESP32_CERTIFICATEHANDLER_H
