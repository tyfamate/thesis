/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-04-09
 * tyfamate@fit.cvut.cz
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#ifndef PUF_IN_TLS_ON_ESP32_CERTIFICATENVS_H
#define PUF_IN_TLS_ON_ESP32_CERTIFICATENVS_H

/**
 * @brief Defines max length of supported hostname
 */
#define HOST_NAME_MAX_LENGTH 128

/**
 * @brief Defines name of NVS partition where hostname is stored
 */
#define HOST_NAME_NVS_NAME "certificate"

/**
 * @brief Defines name of NVS key for hostname
 */
#define HOST_NAME_NVS_KEY "hostname"

#endif //PUF_IN_TLS_ON_ESP32_CERTIFICATENVS_H
