/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-03-14
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf) and wolfSSL (embedded TLS library).
 * With the help of official esp-idf [1] and wolfSSL [2] documentation, wolfSSL source code [3] and wolfSSL examples [4].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://www.wolfssl.com/docs/wolfssl-manual/ and https://www.wolfssl.com/doxygen/
 * [3] https://github.com/wolfSSL/wolfssl
 * [4] https://github.com/wolfSSL/wolfssl-examples
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#ifndef PUF_IN_TLS_ON_ESP32_CERTIFICATECOMMUNICATION_H
#define PUF_IN_TLS_ON_ESP32_CERTIFICATECOMMUNICATION_H

#include <stdbool.h>

/**
 * @brief Struct holding a UART buffer
 */
typedef struct SUART_BUFFER {
    char * const m_buffer; // pointer of the buffer
    const int m_size; // total size of buffer
    int m_used; // current number of characters in buffer
} SUART_BUFFER;

/**
 * @brief Setups UART interface
 * @return TRUE on success
 */
bool configureUART();

/**
 * @brief frees UART interface
 * @return TRUE on success
 */
bool freeUART();

/**
 * @brief Generates a CSR with private key and sends it over serial
 * @param uart pointer to be used as UART buffer
 * @return TRUE on success
 */
bool createCSR(SUART_BUFFER *uart);

/**
 * @brief Receives signed certificate over serial
 * @param buffer pointer to be used as UART buffer
 * @return TRUE on success
 */
bool receiveCertificates(SUART_BUFFER *buffer);

#endif //PUF_IN_TLS_ON_ESP32_CERTIFICATECOMMUNICATION_H
