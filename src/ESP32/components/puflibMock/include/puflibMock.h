/**
 * This component is a mock of a esp32_puflib library [1] developed by Ondřej Staníček as part
 * of his Bachelor's thesis [2]. It is intended to aid with development and to act as a best case scenario.
 * The interface is identical to the original library [1].
 * With the help of Espressif IoT Development Framework (esp-idf) [2].
 *
 * [1] https://github.com/Cpt-Hook/esp32_puflib
 * [2] Ondřej Staníček: Physical unclonable functions on ESP32, Bachelor’s thesis.
 *     Czech Technical University in Prague, Faculty of Information Technology, 2022
 * [3] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 */

#ifndef PUF_IN_TLS_ON_ESP32_PUFLIBMOCK_H
#define PUF_IN_TLS_ON_ESP32_PUFLIBMOCK_H

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

/**
 * @brief Status codes of PUF_RESPONSE and PUF_RESPONSE_LEN
 */
enum PufState {
    RESPONSE_CLEAN, // PUF response is not valid
    RESPONSE_READY, // PUF response is valid
};

#define PUF_STATE PUF_STATE_MOCK
/**
 * @brief Current state of PUF_RESPONSE and PUF_RESPONSE_LEN
 */
extern enum PufState PUF_STATE;

#define PUF_RESPONSE PUF_RESPONSE_MOCK
/**
 * @brief Buffer with PUF response, valid if PUF_STATE == RESPONSE_READY
 */
extern uint8_t *PUF_RESPONSE;

#define PUF_RESPONSE_LEN PUF_RESPONSE_LEN_MOCK
/**
 * @brief Length of PUF_RESPONSE, valid if PUF_STATE == RESPONSE_READY
 */
extern size_t PUF_RESPONSE_LEN;

#define puflib_init puflib_init_mock
/**
 * @brief Init the library, needs to be called as first function, create response if deepsleep used
 */
void puflib_init();

#define get_puf_response get_puf_response_mock
/**
 * @brief Create PUF response, stored in PUF_RESPONSE + PUF_RESPONSE_LEN
 * @return TRUE on success
 */
bool get_puf_response();

#define get_puf_response_reset get_puf_response_reset_mock
/**
 * @brief Create PUF via deepsleep
 */
_Noreturn void get_puf_response_reset();

#define clean_puf_response clean_puf_response_mock
/**
 * @brief Sanitize PUF response memory
 */
void clean_puf_response();

#define provision_puf provision_puf_mock
/**
 * @brief Provisioning of PUF, saves error checking and correction
 */
void provision_puf();

#define puflib_wake_up_stub puflib_wake_up_stub_mock
/**
 * @brief Function to call from deepsleep wakeup stub, facilitates deepsleep
 */
void puflib_wake_up_stub();

#endif //PUF_IN_TLS_ON_ESP32_PUFLIBMOCK_H
