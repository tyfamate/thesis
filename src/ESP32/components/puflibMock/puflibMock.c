/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-04-13
 * tyfamate@fit.cvut.cz
 *
 * This component is a mock of a esp32_puflib library [1] developed by Ondřej Staníček as part
 * of his Bachelor's thesis [2]. The interface is designed by Staníček.
 * It is intended to aid with development and to act as a best case scenario.
 * The interface is identical to the original library [1].
 * With the help of Espressif IoT Development Framework (esp-idf) [2].
 *
 * [1] https://github.com/Cpt-Hook/esp32_puflib
 * [2] Ondřej Staníček: Physical unclonable functions on ESP32, Bachelor’s thesis.
 *     Czech Technical University in Prague, Faculty of Information Technology, 2022
 * [3] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#include "puflibMock.h"

#include <esp_system.h>

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <esp_log.h>
#include <esp_sleep.h>

static const char *TAG = "Mock PUF";

#define RAW_RESPONSE_LENGTH 64

static bool RTC_DATA_ATTR g_Rebooting = false;

enum PufState PUF_STATE = RESPONSE_CLEAN;
uint8_t *PUF_RESPONSE;
size_t PUF_RESPONSE_LEN;

static const uint8_t RAW_RESPONSE[RAW_RESPONSE_LENGTH] = {0xD3, 0xDF, 0xBC, 0x45, 0x5B, 0xC1, 0x22, 0xA6, 0xEA, 0x31,
                                                          0xA9, 0x17, 0x46, 0x44, 0x91, 0x1B, 0xFF, 0x33, 0x89, 0xDE,
                                                          0xC7, 0x60, 0x3C, 0x41, 0x85, 0x52, 0xD0, 0xC1, 0x39, 0xE7,
                                                          0xE7, 0xAE, 0xC4, 0x80, 0x5E, 0xCA, 0xBB, 0x33, 0x19, 0xA6,
                                                          0xC3, 0x34, 0x89, 0x48, 0xB5, 0x8E, 0x8E, 0xB8};

/**
 * @brief Allocates and fills response
 * @return TRUE on success
 */
bool setResponse() {
    PUF_RESPONSE = malloc(RAW_RESPONSE_LENGTH);
    if (!PUF_RESPONSE) {
        ESP_LOGE(TAG, "Unable to allocate space for response");
        return false;
    }

    memcpy(PUF_RESPONSE, RAW_RESPONSE, RAW_RESPONSE_LENGTH);
    PUF_RESPONSE_LEN = RAW_RESPONSE_LENGTH;
    PUF_STATE = RESPONSE_READY;
    ESP_LOGI(TAG, "PUF response ready");
    return true;
}

void puflib_init() {
    PUF_RESPONSE = NULL;
    PUF_RESPONSE_LEN = 0;
    PUF_STATE = RESPONSE_CLEAN;

    if (g_Rebooting) {
        ESP_LOGI(TAG, "Response ready after reboot");
        setResponse();
        g_Rebooting = false;
    }
}

bool get_puf_response() {
    ESP_LOGI(TAG, "Getting response from mock PUF");
    if (PUF_STATE == RESPONSE_READY) {
        ESP_LOGE(TAG, "Response requested when it was already ready");
        return true;
    }

//    if (esp_random() > UINT32_MAX / 2) {
//        ESP_LOGI(TAG, "Requires reboot");
//        g_Rebooting = true;
//        return false;
//    }

    setResponse();
    return true;
}

_Noreturn void get_puf_response_reset() {
    ESP_LOGI(TAG, "Restarting esp for mock PUF");
    esp_sleep_enable_timer_wakeup(50000);
    esp_sleep_pd_config(ESP_PD_DOMAIN_RTC_PERIPH, ESP_PD_OPTION_OFF);
    esp_deep_sleep_start();
}

void clean_puf_response() {
    ESP_LOGI(TAG, "Clearing mock PUF");
    memset(PUF_RESPONSE, 0, RAW_RESPONSE_LENGTH);
    free(PUF_RESPONSE);
    PUF_RESPONSE = NULL;
    PUF_RESPONSE_LEN = 0;
    PUF_STATE = RESPONSE_CLEAN;
    ESP_LOGI(TAG, "PUF response cleared");
}

void provision_puf() {
    ESP_LOGI(TAG, "Provisioning mock PUF");
}

void RTC_IRAM_ATTR puflib_wake_up_stub() {
    //ESP_LOGI(TAG, "Mock PUF Wakeup stub called");
}
