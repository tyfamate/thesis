/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-19
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf),
 * with the help of official documentation [1], example ESP-IDF project [2] and RFC1035[3].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/index.html#wi-fi
 * [2] https://github.com/espressif/esp-idf/tree/master/examples/wifi/getting_started/station
 * [2] https://www.rfc-editor.org/info/rfc1035
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#include "accessPoint.h"

#include <string.h>

#include <esp_check.h>
#include <esp_system.h>
#include <esp_wifi.h>
#include <esp_event.h>
#include <esp_log.h>

// Set tag for logging library
static const char *TAG = "Access point";

#define SSID    CONFIG_WIFI_SSID
#define PSK     CONFIG_WIFI_PSK
#define CHANNEL CONFIG_WIFI_CHANNEL

/**
 * @brief Holds the IP info of ESP
 */
static esp_netif_ip_info_t g_ESP_IP;

const esp_netif_ip_info_t *getIP(void) {
    return &g_ESP_IP;
}

void accessPointInit(void) {
    ESP_LOGD(TAG, "AP start requested");

    esp_netif_t *netif = esp_netif_create_default_wifi_ap();
    esp_netif_get_ip_info(netif, &g_ESP_IP);
    ESP_LOGI(TAG, "DNS is responding with IP: "IPSTR, IP2STR(&g_ESP_IP.ip));

    // Init WiFi with default config -> abort on fail
    wifi_init_config_t initConfig = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&initConfig));
    ESP_LOGV(TAG, "[success] init");

    // Set WiFi to Soft AP == access point
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_AP));
    ESP_LOGV(TAG, "[success] set mode to AP");


    wifi_config_t config = {
            .ap = {
                    .ssid = SSID,
                    .password = PSK,
                    .ssid_len = strlen(SSID),
                    .channel = CHANNEL,
                    .authmode = WIFI_AUTH_WPA2_PSK,
                    .ssid_hidden = false,
                    .max_connection = 4,
                    .beacon_interval = 100,
                    .pairwise_cipher = WIFI_CIPHER_TYPE_CCMP,
                    .ftm_responder = false,
                    .pmf_cfg = {
                            .capable = true,
                            .required = false
                    }
            }
    };

    if (strlen(PSK) == 0) {
        config.ap.authmode = WIFI_AUTH_OPEN;
    }

    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_AP, &config));
    ESP_LOGV(TAG, "[success] set config");

    // Start AP
    ESP_ERROR_CHECK(esp_wifi_start());
    ESP_LOGV(TAG, "[success] start");

    ESP_LOGI(TAG, "AP activated SSID=\"%s\" PSK=\"%s\" CHANNEL=%d", SSID, PSK, CHANNEL);
}

void accessPointFree() {
    esp_wifi_stop();
    esp_wifi_deinit();
}
