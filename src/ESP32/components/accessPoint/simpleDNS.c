/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-19
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf),
 * with the help of official documentation [1], example ESP-IDF project [2] and RFC1035[3].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/index.html#wi-fi
 * [2] https://github.com/espressif/esp-idf/tree/master/examples/wifi/getting_started/station
 * [2] https://www.rfc-editor.org/info/rfc1035
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#define LOG_LOCAL_LEVEL ESP_LOG_DEBUG

#include "simpleDNS.h"
#include "accessPoint.h"
#include "certificateNVS.h"

#include <string.h>
#include <sys/socket.h>

#include <esp_check.h>
#include <esp_system.h>
#include <esp_log.h>
#include <nvs_flash.h>

// Set tag for logging library
static const char *TAG = "simple DNS";

#define SSID    CONFIG_WIFI_SSID
#define PSK     CONFIG_WIFI_PSK
#define CHANNEL CONFIG_WIFI_CHANNEL

/**
 * @brief What port to run DNS at
 */
#define DNS_PORT 53

/**
 * @brief What is the max size of DNS request
 */
#define DNS_REQUEST_SIZE 128

/**
 * @brief Holds devices hostname
 */
static char g_HOST_NAME[HOST_NAME_MAX_LENGTH];

/**
 * @brief Holds length of hostname
 */
static size_t g_HOST_NAME_LENGTH = 0;

/**
 * @brief Holds IP of server connected with hostname
 */
static const esp_netif_ip_info_t *g_SERVER_IP;

/**
 * @brief Struct that holds DNS header
 */
typedef struct {
    uint16_t m_id;
    uint16_t m_flags; // QR + OPCODE + AA + TC + RD + RA + Z + RCODE
    uint16_t m_questionCount;
    uint16_t m_answerCount;
    uint16_t m_authorityRecordCount;
    uint16_t m_additionalRecordCount;
} __attribute__((packed)) SDNSHeader;

/**
 * @brief Struct that holds DNS IPV4 answer
 */
typedef struct {
    uint16_t m_namePointer; // 11xxxxxx xxxxxxxx pointer to a previous name
    uint16_t m_type;
    uint16_t m_class;
    uint32_t m_ttl;
    uint16_t m_length;
    struct in_addr m_address;
} __attribute__((packed)) SDNSRecord;

/**
 * @brief Sets DNS header to RCODE = 0 = all ok
 * @param dnsHeader [in/out] header to be modified
 */
static inline void setDNSOK(SDNSHeader *dnsHeader) {
    ESP_LOGI(TAG, "DNS request successfully handled");
    if (dnsHeader) // Modify header to response + no recursion available + Z=000 + RCODE=0 (ok)
        dnsHeader->m_flags = htons((ntohs(dnsHeader->m_flags) & 0b0111101100000000) + 0b1000000000000000);
}

/**
 * @brief Sets DNS header to RCODE = 1 = format error
 * @param dnsHeader [in/out] header to be modified
 */
static inline void setDNSFormatError(SDNSHeader *dnsHeader) {
    ESP_LOGD(TAG, "Malformed DNS request received");
    if (dnsHeader) // Modify header to response + no recursion available + Z=000 + RCODE=1 (format error)
        dnsHeader->m_flags = htons((ntohs(dnsHeader->m_flags) & 0b0111101100000000) + 0b1000000000000001);
}

/**
 * @brief Sets DNS header to RCODE = 4 = not implemented
 * @param dnsHeader [in/out] header to be modified
 */
static inline void setDNSNotImplementedError(SDNSHeader *dnsHeader) {
    ESP_LOGD(TAG, "Unsupported DNS request received");
    if (dnsHeader) // Modify header to response + no recursion available + Z=000 + RCODE=4 (not implemented)
        dnsHeader->m_flags = htons((ntohs(dnsHeader->m_flags) & 0b0111101100000000) + 0b1000000000000100);
}

/**
 * @brief Sets DNS header to RCODE = 5 = refused
 * @param dnsHeader [in/out] header to be modified
 */
static inline void setDNSRefusedError(SDNSHeader *dnsHeader) {
    ESP_LOGV(TAG, "Refused DNS request");
    if (dnsHeader) // Modify header to response + no recursion available + Z=000 + RCODE=5 (refused)
        dnsHeader->m_flags = htons((ntohs(dnsHeader->m_flags) & 0b0111101100000000) + 0b1000000000000101);
}

/**
 * @brief Checks if QNAME in DNS question matches expected hostname
 * @param dnsQuestion [in] question to be checked
 * @param questionLength [in] length of question
 * @param hostNameLength [out] length of hostname field in question
 * @return TRUE on match
 */
static bool doesHostNameMatch(const uint8_t * const dnsQuestion, size_t questionLength, size_t *hostNameLength) {
    if (!dnsQuestion || !hostNameLength)
        return false;

    bool found = g_HOST_NAME_LENGTH ? true : false;
    uint8_t hostNameCurrentPosition = 0;
    for (*hostNameLength = 0; *hostNameLength + 5 < questionLength && dnsQuestion[*hostNameLength]
                         && *hostNameLength + dnsQuestion[*hostNameLength] < questionLength
            ; *hostNameLength += dnsQuestion[*hostNameLength] + 1) {
        if (!found) // only find length of QNAME
            continue;

        uint8_t QNAMELength = dnsQuestion[*hostNameLength];
        char *QNAME = (char *)&(dnsQuestion[*hostNameLength + 1]);

        if (hostNameCurrentPosition >= g_HOST_NAME_LENGTH
                || (hostNameCurrentPosition && g_HOST_NAME[hostNameCurrentPosition++] != '.')) { // not first segment -> should be .
            found = false;
            break;
        }

        for (size_t i = 0; i < QNAMELength; ++i) {
            if (hostNameCurrentPosition >= g_HOST_NAME_LENGTH || QNAME[i] != g_HOST_NAME[hostNameCurrentPosition++]) {
                found = false;
                break;
            }
        }
    }

    return found;
}

/**
 * @brief Answers one DNS question
 * @param dnsHeader [in/out] pointer to header of question - will be modified based on success
 * @param dnsQuestion [in] pointer to question
 * @param questionLength [in] length of question
 * @param dnsRecord [out] where to place answer
 * @return TRUE on success
 */
static bool answerDNSQuestion(SDNSHeader *dnsHeader, const uint8_t * const dnsQuestion, size_t questionLength, SDNSRecord *dnsRecord) {
    ESP_LOGV(TAG, "Answering DNS question");

    size_t hostNameLength = 0;
    bool matches = doesHostNameMatch(dnsQuestion, questionLength, &hostNameLength);

    if(hostNameLength + 1 + 2 + 2 != questionLength) { // out of bounds - only last octet + QTYPE + QCLASS should be remaining
        setDNSFormatError(dnsHeader);
        return false;
    }

    uint16_t qtype = ntohs(*(uint16_t *)(dnsQuestion + hostNameLength + 1));
    uint16_t qclass = ntohs(*(uint16_t *)(dnsQuestion + hostNameLength + 1 + 2));
    if (qtype != 0x01 || qclass != 0x01) { // only type==A + qclass==IN supported
        setDNSNotImplementedError(dnsHeader);
        return false;
    }

    if (!matches) {
        setDNSRefusedError(dnsHeader);
        return false;
    }

    ESP_LOGV(TAG, "DNS question match found");
    *dnsRecord = (SDNSRecord) {
            .m_namePointer = htons(0xc00c), // use name of first query
            .m_type = htons(0x01), // A record
            .m_class = htons(0x01), // IN - internet class
            .m_ttl = htons(3600),
            .m_length = htons(4), // one IPV4 address
            .m_address = {
                    g_SERVER_IP->ip.addr
                    },
    };

    setDNSOK(dnsHeader);
    dnsHeader->m_answerCount = htons(0x0001); // Set answer count to 1
    return true;
}

/**
 * @brief Handles one DNS request
 * @param request [in/out] DNS request - modified with answer
 * @param requestLength [in/out] length of request/answer
 */
static void handleDNSRequest(uint8_t *request, ssize_t *requestLength) {
    if (!request || !requestLength)
        return;

    SDNSHeader *dnsHeader = (SDNSHeader *)request;
    dnsHeader->m_answerCount = htons(0x0000); // Set answer count to 0

    // Check if header is containing expected values -> if not return error
    // expected 0b0000000XX0XXXX = standard query + N AA + N TC + X RD + X RA + Z=000 + X RCODE
    if (ntohs(dnsHeader->m_questionCount == 0) || ntohs(dnsHeader->m_authorityRecordCount) != 0
        || ntohs(dnsHeader->m_additionalRecordCount) != 0 || (ntohs(dnsHeader->m_flags) & 0b1111111001000000) != 0) {
        setDNSFormatError(dnsHeader);
    } else if (ntohs(dnsHeader->m_questionCount) != 1 || *requestLength < sizeof(SDNSHeader) + 1 + 2 + 2) {
        // only one question is supported (HEADER + QNAME_len + ... + QTYPE + QCLASS)
        setDNSNotImplementedError(dnsHeader);
    } else { // Header OK
        uint8_t *dnsQuestion = &(request[sizeof(SDNSHeader)]);
        if (answerDNSQuestion(dnsHeader, dnsQuestion, *requestLength - sizeof(SDNSHeader), (SDNSRecord *)&(request[*requestLength]))) {
            *requestLength += sizeof(SDNSRecord);
        }
    }
}

/**
 * @brief Infinite loop for DNS handling
 * @param socketFD UDP socket descriptor
 */
_Noreturn static void mockDNSLoop(int socketFD) {
    uint8_t* request = (uint8_t *)malloc(sizeof(uint8_t) * DNS_REQUEST_SIZE + sizeof(SDNSRecord));

    while (true) {
        struct sockaddr_in clientAddress;
        memset(&clientAddress, 0, sizeof(clientAddress));
        socklen_t clientAddressLen = sizeof(clientAddress);
        ssize_t requestLength = recvfrom(socketFD, request, DNS_REQUEST_SIZE, MSG_WAITALL, (struct sockaddr *)&clientAddress, &clientAddressLen);
        ESP_LOGV(TAG, "Got DNS request");

        if (requestLength >= sizeof(SDNSHeader)) {
            handleDNSRequest(request, &requestLength);
        }

        ESP_LOGV(TAG, "Sending DNS response");
        if (sendto(socketFD, request, requestLength, 0, (const struct sockaddr *) &clientAddress, sizeof(clientAddress)) != requestLength) {
            ESP_LOGE(TAG, "Unable to send DNS response");
        }
    }
}

/**
 * @brief Loads a hostname from NVS to global variable
 */
static void getHostName() {
    nvs_handle_t handle;

    if (nvs_open(HOST_NAME_NVS_NAME, NVS_READONLY, &handle) != ESP_OK) {
        ESP_LOGE(TAG, "Unable to open NVS to get hostname");
        return;
    }

    g_HOST_NAME_LENGTH = HOST_NAME_MAX_LENGTH;
    if (nvs_get_str(handle, HOST_NAME_NVS_KEY, g_HOST_NAME, &g_HOST_NAME_LENGTH) != ESP_OK) {
        ESP_LOGE(TAG, "Unable to get hostname from NVS");
        g_HOST_NAME_LENGTH = 0;
    }

    nvs_close(handle);
}

//======================================================================================================================
// PUBLIC FACING FUNCTIONS
//======================================================================================================================
void simpleDNSRun(void * var) {
    ESP_LOGI(TAG, "Simple mock DNS starting");
    g_SERVER_IP = getIP();
    if (!g_SERVER_IP) {
        ESP_LOGE(TAG, "Unable to get server IP, disabling DNS");
        vTaskDelete(NULL);
        return;
    }

    getHostName();

    int socketFD = socket(AF_INET, SOCK_DGRAM, 0);
    if (socketFD == -1) {
        ESP_LOGE(TAG, "Unable to create DNS socket, disabling DNS");
        vTaskDelete(NULL);
        return;
    }
    ESP_LOGV(TAG, "DNS socket created");

    struct sockaddr_in serverAddress;
    memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(DNS_PORT);
    serverAddress.sin_addr.s_addr = INADDR_ANY;

    int returnVal = bind(socketFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
    if (returnVal == -1) {
        ESP_LOGE(TAG, "Unable to bind DNS socket, disabling DNS");
        vTaskDelete(NULL);
        return;
    }
    ESP_LOGV(TAG, "DNS socket bind successful");

    mockDNSLoop(socketFD);
}