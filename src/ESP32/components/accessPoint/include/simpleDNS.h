/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-19
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf),
 * with the help of official documentation [1] and example ESP-IDF project [2].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/index.html#wi-fi
 * [2] https://github.com/espressif/esp-idf/blob/master/examples/wifi/getting_started/station/main/station_example_main.c
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#ifndef PUF_IN_TLS_ON_ESP32_SIMPLEDNS_H
#define PUF_IN_TLS_ON_ESP32_SIMPLEDNS_H

/**
 * @brief Runs a simple DNS that answers with devices IP for A request of common name from certificate
 */
void simpleDNSRun(void *);

#endif //PUF_IN_TLS_ON_ESP32_SIMPLEDNS_H
