/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-19
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf),
 * with the help of official documentation [1] and example ESP-IDF project [2].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/api-reference/network/index.html#wi-fi
 * [2] https://github.com/espressif/esp-idf/blob/master/examples/wifi/getting_started/station/main/station_example_main.c
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#ifndef PUF_IN_TLS_ON_ESP32_ACCESSPOINT_H
#define PUF_IN_TLS_ON_ESP32_ACCESSPOINT_H

#include <esp_netif.h>

/**
 * @brief Initializes wifi in AP mode
 */
void accessPointInit();

/**
 * @brief Disables wifi
 */
void accessPointFree();

/**
 * @brief Gets IP address of ESP
 * @return IP address of ESP
 */
const esp_netif_ip_info_t *getIP(void);

#endif //PUF_IN_TLS_ON_ESP32_ACCESSPOINT_H
