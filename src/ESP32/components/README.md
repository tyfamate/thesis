# This directory
In this directory you can find multiple components that make up this application.

In each folder you can find separate component.

## wolfssl
[wolfssl](./wolfssl) is a library developed by [wolfSSL Inc.](https://www.wolfssl.com/) with source code available on [GitHub](https://github.com/wolfSSL/wolfssl)

#### Version
[`wolfSSL Release 5.2.0 (Feb 21, 2022)`](https://github.com/wolfSSL/wolfssl/releases/tag/v5.2.0-stable)

### Copyright
This source code was released under GPL Version 2 license. Which full text can be found in [COPYRIGHT](wolfssl/COPYRIGHT).

### License
This is not my work. Original license can be found in [LICENSE](wolfssl/LICENSE).

### How to acquire this library
0. Install [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/) framework
1. Visit [wolfSSL GitHub](https://github.com/wolfSSL/wolfssl/releases/tag/v5.2.0-stable)
2. Download `Source code (zip)` or `Source code (tar.gz)`
3. Unpack the archive
4. navigate to `wolfssl-5.2.0-stable/IDE/Espressif/ESP-IDF/`
5. run `setup.sh` on GNU/Linux or `setup_win.bat` on Windows
6. Files can be found at `/PATH/TO/ESP-IDF/esp-idf/components/wolfssl`

### Modifications
For my purposes only [CMakeLists.txt](wolfssl/CMakeLists.txt) build instructions were modified.

Original file can be found in [ORIGINAL_CMakeLists.txt](wolfssl/ORIGINAL_CMakeLists.txt).

#### Modifications TLDR
- Relative link up single directory `..` was replaced by `${IDF_PATH}/components` a relative path to ESP-IDF root.

- Include path for `user_settings.h` was moved from `./include` to `../../wolfSSLConfiguration`


## puflib
[puflib](./puflib) is a library developed by Ondřej Staníček with source code available on [GitHub](https://github.com/Cpt-Hook/esp32_puflib).
As part of: Physical unclonable functions on ESP32, Bachelor’s thesis, Czech Technical University in Prague, Faculty of Information Technology, 2022

#### Version
puflib commit ["Remove excess function call in wake up stub and unneeded function"](https://github.com/Cpt-Hook/esp32_puflib/commit/572fe9ae8fff7fbdfa600f445017a55aa5af2904) (Mar 22, 2022)

### Copyright
Copyright notice was not available.

### License
This is not my work. However, Ondřej Staníček has given me permission to use his work.

### How to acquire this library
0. Install [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/) framework
1. Download repository [as a zip](https://github.com/Cpt-Hook/esp32_puflib/archive/refs/heads/main.zip) or [clone with git (HTTPS)](https://github.com/Cpt-Hook/esp32_puflib.git)
2. Unpack the archive (if downloaded as zip)
3. Move the downloaded (unpacked) folder to this `component` folder as `puflib`

### Modifications
The file [puf_measurement.c](./puflib/puf_measurement.c), at line 72 (73 after modifications) calling `ets_delay_us(sleep_us);` which were not included. We therfore included `#include <rom/ets_sys.h>` at the top of the file.

The file [puflib.h](./puflib/include/puflib.h), at line 19 defines `const extern enum PufState PUF_STATE;`, we modified by removing the `const` specifier.

I therefore added `#include <rom/ets_sys.h>` on line 11.
