# wolfSSL library source code
In this component you can find a source code of wolfSSL – an Embedded TLS Library.

This library is developed by [wolfSSL Inc.](https://www.wolfssl.com/) with source code available on [GitHub](https://github.com/wolfSSL/wolfssl)

### Version
[`wolfSSL Release 5.2.0 (Feb 21, 2022)`](https://github.com/wolfSSL/wolfssl/releases/tag/v5.2.0-stable)

## Copyright
This source code was released under GPL Version 2 license. Which full text can be found in [COPYRIGHT](COPYRIGHT).

## License
This is not my work. Original license can be found in [LICENSE](LICENSE).

## How to acquire this library
0. Install [ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/latest/get-started/) framework
1. Visit [wolfSSL GitHub](https://github.com/wolfSSL/wolfssl/releases/tag/v5.2.0-stable)
2. Download `Source code (zip)` or `Source code (tar.gz)`
3. Unpack the archive
4. navigate to `wolfssl-5.2.0-stable/IDE/Espressif/ESP-IDF/`
5. run `setup.sh` on GNU/Linux or `setup_win.bat` on Windows
6. Files can be found at `/PATH/TO/ESP-IDF/esp-idf/components/wolfssl`

## Modifications
For my purposes only [CMakeLists.txt](CMakeLists.txt) build instructions were modified.

Original file can be found in [ORIGINAL_CMakeLists.txt](ORIGINAL_CMakeLists.txt).

### Modifications TLDR
- Relative link up single directory `..` was replaced by `${IDF_PATH}/components` a relative path to ESP-IDF root.

- Include path for `user_settings.h` was moved from `./include` to `../../wolfSSLConfiguration`