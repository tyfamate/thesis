/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-28
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf) and wolfSSL (embedded TLS library),
 * with the help of official esp-idf [1] and wolfSSL [2] documentation, wolfSSL source code [3].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://www.wolfssl.com/docs/wolfssl-manual/ and https://www.wolfssl.com/doxygen/
 * [3] https://github.com/wolfSSL/wolfssl
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#include "webserver_WolfSSL.h"
#include "webserver_defines.h"

#include <stdlib.h>

#include <esp_log.h>
#include <esp_check.h>

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/ssl.h>
#include <wolfssl/wolfcrypt/logging.h>

static const char *TAG = "webserver WolfSSL";

#define MAX_CONNECTIONS CONFIG_MAX_CONNECTIONS
#define TIME_BUFFER_LENGTH 32

/**
 * @brief Handle HTTP request on root endpoint
 * @param ssl [in] TLS/SSL handle of connection
 */
static void rootHandler(WOLFSSL *ssl) {
    if (!ssl)
        return;
    ESP_LOGD(TAG, "Serving root");

    int msgLen = (int)strlen(g_ROOT_RESPONSE_FIRST);
    if (wolfSSL_write(ssl, g_ROOT_RESPONSE_FIRST, msgLen) != msgLen) {
        ESP_LOGE(TAG, "Unable to send root first part");
    }

    char buffer[TIME_BUFFER_LENGTH];
    int timeLength = snprintf(buffer, TIME_BUFFER_LENGTH,"%lld", esp_timer_get_time() / 1000000); // microseconds to seconds
    if (wolfSSL_write(ssl, buffer, timeLength) != timeLength) {
        ESP_LOGE(TAG, "Unable to send root time part");
    }

    msgLen = (int)strlen(g_ROOT_RESPONSE_LAST);
    if (wolfSSL_write(ssl, g_ROOT_RESPONSE_LAST, msgLen) != msgLen) {
        ESP_LOGE(TAG, "Unable to send root last part");
    }
}

/**
 * @brief Handle HTTP request for favicon
 * @param ssl [in] TLS/SSL handle of connection
 */
static void faviconHandler(WOLFSSL *ssl) {
    if (!ssl)
        return;
    ESP_LOGD(TAG, "Serving favicon");
    int msgLen = (int)strlen(g_FAVICON_RESPONSE_HEADER);
    if (wolfSSL_write(ssl, g_FAVICON_RESPONSE_HEADER, msgLen) != msgLen) {
        ESP_LOGE(TAG, "Unable to send favicon headers");
    }
    if (wolfSSL_write(ssl, g_FAVICON_START, (int)(g_FAVICON_END - g_FAVICON_START)) != g_FAVICON_END - g_FAVICON_START) {
        ESP_LOGE(TAG, "Unable to send favicon data");
    }
}

/**
 * @brief Handle HTTP request on unknown endpoint
 * @param ssl [in] TLS/SSL handle of connection
 */
static void notFoundHandler(WOLFSSL *ssl) {
    if (!ssl)
        return;

    ESP_LOGD(TAG, "Serving 404");
    int msgLen = (int)strlen(g_404_RESPONSE);
    if (wolfSSL_write(ssl, g_404_RESPONSE, msgLen) != msgLen) {
        ESP_LOGE(TAG, "Unable to send 404");
    }
}

//======================================================================================================================
// PUBLIC FACING FUNCTIONS
//======================================================================================================================
void handleHTTPSCommunication(WOLFSSL *ssl) {
    ESP_LOGI(TAG, "Webserver with WolfSSL starting");
    if (!ssl)
        return;
    ESP_LOGD(TAG, "start Server - Client communication");

    static const int maxRequestSize = 1024;
    char request[maxRequestSize];


    int requestSize = wolfSSL_read(ssl, request, maxRequestSize - 1);
    if (requestSize <= 0) {
        ESP_LOGE(TAG, "Read error \'%d\'", wolfSSL_get_error(ssl, requestSize));
    }
    request[requestSize] = '\0';
    WOLFSSL_MSG("From client:");
    WOLFSSL_MSG(request);

    char *pathSave = request;
    char *type = strtok_r(request, " ", &pathSave);
    if (type && !strcmp(type, REQUEST_GET)) {
        WOLFSSL_MSG("Requested type:");
        WOLFSSL_MSG(type);

        char *path = strtok_r(NULL, " ", &pathSave);
        if (path) {
            WOLFSSL_MSG("Requested path:");
            WOLFSSL_MSG(path);

            if (!strcmp(path, PATH_ROOT)) {
                rootHandler(ssl);
            } else if (!strcmp(path, PATH_FAVICON)) {
                faviconHandler(ssl);
            } else {
                notFoundHandler(ssl);
            }
        }
    }
}