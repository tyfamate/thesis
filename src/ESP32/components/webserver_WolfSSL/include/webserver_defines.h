/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-28
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf) and wolfSSL (embedded TLS library),
 * with the help of official esp-idf [1] and wolfSSL [2] documentation, wolfSSL source code [3].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://www.wolfssl.com/docs/wolfssl-manual/ and https://www.wolfssl.com/doxygen/
 * [3] https://github.com/wolfSSL/wolfssl
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#ifndef PUF_IN_TLS_ON_ESP32_WEBSERVER_DEFINES_H
#define PUF_IN_TLS_ON_ESP32_WEBSERVER_DEFINES_H

#include <stdint.h>

/**
 * @brief Get request identifier in HTTP header
 */
#define REQUEST_GET "GET"

/**
 * @brief Root path identifier in HTTP header
 */
#define PATH_ROOT "/"

/**
 * @brief first part of HTTP response to be served as root
 */
static const char g_ROOT_RESPONSE_FIRST[] = "HTTP/1.1 200 OK\r\n"
                                            "Content-Type: text/html\r\n"
                                            "Connection: close\r\n"
                                            "Content-Length: 143\r\n"
                                            "\r\n"
                                            "<html>\r\n"
                                            "<head>\r\n"
                                            "<title>PUF in TLS on ESP32</title>\r\n"
                                            "</head>\r\n"
                                            "<body>\r\n"
                                            "<h1>ESP uptime is: ";

/**
 * @brief last part of HTTP response to be server as root
 */
static const char g_ROOT_RESPONSE_LAST[] = " s</h1>\r\n"
                                           "</body>\r\n"
                                           "</html>\r\n";

/**
 * @brief Favicon ath identifier in HTTP header
 */
#define PATH_FAVICON "/favicon.ico"

/**
 * @brief HTTP response header to be served as favicon
 */
static const char g_FAVICON_RESPONSE_HEADER[] = "HTTP/1.1 200 OK\r\n"
                                                "Content-Type: image/png\r\n"
                                                "Connection: close\r\n"
                                                "Content-Encoding"
                                                "Content-Length: 707\r\n"
                                                "\r\n";

/**
 * @brief Start of favicon binary representation
 */
extern const uint8_t g_FAVICON_START[] asm("_binary_favicon_png_start");

/**
 * @brief End of favicon as binary representation
 */
extern const uint8_t g_FAVICON_END[]   asm("_binary_favicon_png_end");

/**
 * @brief HTTP response to be served as not found
 */
static const char g_404_RESPONSE[] = "HTTP/1.1 404 Not Found\r\n"
                                     "Content-Type: text/plain\r\n"
                                     "Connection: close\r\n"
                                     "Content-Length: 0\r\n";

#endif //PUF_IN_TLS_ON_ESP32_WEBSERVER_DEFINES_H
