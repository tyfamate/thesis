/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-28
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf) and wolfSSL (embedded TLS library),
 * with the help of official esp-idf [1] and wolfSSL [2] documentation, wolfSSL source code [3].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://www.wolfssl.com/docs/wolfssl-manual/ and https://www.wolfssl.com/doxygen/
 * [3] https://github.com/wolfSSL/wolfssl
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#ifndef PUF_IN_TLS_ON_ESP32_TLS_AWAIT_CONNECT_H
#define PUF_IN_TLS_ON_ESP32_TLS_AWAIT_CONNECT_H

#include <wolfssl/ssl.h>

/**
 * @brief Awaits a TCP connection and calls callback when connection and TLS is established
 * @param port port number to await the connection
 * @param maxConnections maximal number of concurrent connections
 * @param callback callback to be called
 */
void tlsAwaitConnect(uint16_t port, uint8_t maxConnections, void (*callback)(WOLFSSL *));

#endif //PUF_IN_TLS_ON_ESP32_TLS_AWAIT_CONNECT_H
