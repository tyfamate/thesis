/**
 * @author  Matěj Týfa (tyfamate)
 * @date    2022-02-28
 * tyfamate@fit.cvut.cz
 *
 * Developed using Espressif IoT Development Framework (esp-idf) and wolfSSL (embedded TLS library),
 * with the help of official esp-idf [1] and wolfSSL [2] documentation, wolfSSL source code [3].
 *
 * [1] https://docs.espressif.com/projects/esp-idf/en/latest/esp32/index.html
 * [2] https://www.wolfssl.com/docs/wolfssl-manual/ and https://www.wolfssl.com/doxygen/
 * [3] https://github.com/wolfSSL/wolfssl
 *
 * Created as part of my Bachelor's thesis:
 * Using physical unclonable functions in TLS on ESP32
 * Czech Technical University in Prague,
 * Faculty of Information Technology,
 * Department of Computer Systems
 * 2022
 */

#include "tlsAwaitConnect.h"
#include "certificateHandler.h"
#include "globalWakeupState.h"

#include <stdlib.h>

#include <esp_log.h>
#include <esp_check.h>

#include <wolfssl/wolfcrypt/settings.h>
#include <wolfssl/ssl.h>
#include <wolfssl/wolfcrypt/logging.h>

static const char *TAG = "wolfSSL TLS Await connect";

/**
 * @brief List of cipher suites to support
 */
#define CIPHER_SUITES_LIST "TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256"

/**
 * @brief List of key exchange groups to support
 */
static int s_GROUPS[] = {WOLFSSL_ECC_SECP256R1, WOLFSSL_ECC_SECP384R1, WOLFSSL_ECC_X25519, WOLFSSL_ECC_BRAINPOOLP256R1, WOLFSSL_ECC_BRAINPOOLP384R1};

/**
 * @brief Count of supported groups
 */
static const int s_GROUPS_COUNT = 5;

/**
 * @brief Handler for single TCP/443 connection
 * @param ctx [in] configured wolfssl context to be used with this connection
 * @param connectionFD [in] socket file descriptor of connection
 */
static void handleConnection(WOLFSSL_CTX *ctx, int connectionFD, void (*callback)(WOLFSSL *)) {
    if (!ctx || !callback)
        return;
    ESP_LOGI(TAG, "Handling connection");

    int returnVal;

    WOLFSSL *ssl = wolfSSL_new(ctx);
    ESP_RETURN_ON_FALSE(ssl, , TAG, "Unable to create TLS handler");

    returnVal = wolfSSL_set_fd(ssl, connectionFD);
    if (returnVal != SSL_SUCCESS) {
        ESP_LOGE(TAG, "Unable to bind socket and TLS \'%d\'", wolfSSL_get_error(ssl, returnVal));
        wolfSSL_free(ssl);
        return;
    }

    returnVal = wolfSSL_accept(ssl);
    if (returnVal != SSL_SUCCESS) {
        ESP_LOGE(TAG, "Unable to establish TLS with error \'%d\' from WolfSSL", wolfSSL_get_error(ssl, returnVal));
        wolfSSL_free(ssl);
        return;
    }
    ESP_LOGD(TAG, "TLS connection established");

    callback(ssl);

    if (wolfSSL_shutdown(ssl) != SSL_SUCCESS)
        ESP_LOGW(TAG, "Unable to gracefully shutdown TLS connection");
    wolfSSL_free(ssl);
}

//======================================================================================================================
// PUBLIC FACING FUNCTIONS
//======================================================================================================================
void tlsAwaitConnect(uint16_t port, uint8_t maxConnections, void (*callback)(WOLFSSL *)) {
    if (!callback)
        return;
    ESP_LOGI(TAG, "awaiting TLS connection");

    int socketFD = socket(AF_INET, SOCK_STREAM, 0);
    ESP_RETURN_ON_FALSE(socketFD != -1, , TAG, "Unable to create socket");
    ESP_LOGV(TAG, "Socket created");

    // Use only TLS 1.3
    WOLFSSL_CTX *ctx = wolfSSL_CTX_new(wolfTLSv1_3_server_method());
    ESP_RETURN_ON_FALSE(ctx != NULL, , TAG, "Unable to create CTX");
    ESP_LOGV(TAG, "Context created");

    int returnVal;

    ESP_RETURN_ON_FALSE(initCertificateStorage(), , TAG, "Unable to initialize certificate storage");
    returnVal = wolfSSL_CTX_use_certificate_chain_file(ctx, getCertificateFilename());
    ESP_RETURN_ON_FALSE(freeCertificateStorage(), , TAG, "Unable to free certificate storage");
    ESP_RETURN_ON_FALSE(returnVal == SSL_SUCCESS, , TAG, "Unable to read certificate");
    ESP_LOGV(TAG, "Certificate read");

    ESP_RETURN_ON_FALSE(wolfSSL_CTX_set_cipher_list(ctx, CIPHER_SUITES_LIST) == SSL_SUCCESS, , TAG,
                        "Unable to set cipher suite");
    ESP_RETURN_ON_FALSE(wolfSSL_CTX_set_groups(ctx, s_GROUPS, s_GROUPS_COUNT) == SSL_SUCCESS, , TAG,
                        "Unable to set key exchange groups");

    long bufferLength = 0;

    enum EWakeupTargets oldWakeup = g_WakeupTarget; // Make a copy of wakeup state to reset it later
    g_WakeupTarget = WEBSERVER_INIT;
    const unsigned char *buffer = getKeyDER(&bufferLength);
    g_WakeupTarget = oldWakeup;

    returnVal = wolfSSL_CTX_use_PrivateKey_buffer(ctx, buffer, bufferLength, SSL_FILETYPE_ASN1);
    freeKeyDER();
    ESP_RETURN_ON_FALSE(returnVal == SSL_SUCCESS, , TAG, "Unable to read private key");
    ESP_LOGV(TAG, "Private key loaded");

    struct sockaddr_in serverAddress;
    memset(&serverAddress, 0, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(port);
    serverAddress.sin_addr.s_addr = INADDR_ANY;

    returnVal = bind(socketFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress));
    ESP_RETURN_ON_FALSE(returnVal == 0, , TAG, "Unable to bind socket");
    ESP_LOGV(TAG, "Socket bind successful");

    returnVal = listen(socketFD, maxConnections);
    ESP_RETURN_ON_FALSE(returnVal == 0, , TAG, "Unable to listen");
    ESP_LOGI(TAG, "Server listening...");

    while (true) {
        struct sockaddr_in clientAddress;
        socklen_t clientAddressLength = sizeof(clientAddress);
        int connectionFD = accept(socketFD, (struct sockaddr *)&clientAddress, &clientAddressLength);
        if (connectionFD < 0) {
            ESP_LOGE(TAG, "Unable to accept connection");
            continue;
        }

        handleConnection(ctx, connectionFD, callback);

        close(connectionFD);
    }
}