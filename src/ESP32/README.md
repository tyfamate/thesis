# Proof of concept application

In this folder is a source code for our proof of conecept application.

In order to compile and flash the app you need to install ESP-IDF.

https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html#installation

We used a version `v5.0-dev-1730-g229ed08484`, however a newer release will most likely work as well.

# Using the app
Afer you flash the app you need to enroll the device.

Connect pin 16 to 3.3v and reset the device. Enrollment process should now
be started. You can remove the connection and continue with the enrollment.

In the enrollment you need to fill fields of the CSR. Example values can be found
in [CSR_default_values.txt](../PC/CSR_default_values.txt).

After that you need to sign the CSR with a CA (either local openssl, https://getacert.com/signacert.html, etc.)

The generated cert (with chain if provided) then transmit back to the device. If chain is provided,
the devices certificate must go first, followed by the rest.

If the enrollment is successfully, you should now be able to connect
to `PUF_in_TLS_WiFi` with password `1234567890`.

You can now connect to `192.168.4.1` on port `443` a see the TLS in action.

If you used a domain name as a common name, it should also point to the server.

# Building
You can build, flash and monitor this application using `idf.py app flash monitor`.

# Configurations

## Defaults

MOCK implementation of the library will be used.

GPIO 16 is used as enrollment trigger.

## Configuration for wolfSSL

WolfSSL is configured using [user_settings.h](./wolfSSLConfiguration/user_settings.h)

## ESP-IDF config + general configuration

All other configuration should be accessible either via `idf.py menuconfig`
or via [sdkconfig.defaults](./sdkconfig.defaults).

If you modify the sdkconfig.defaults you need to then delete [sdkconifg](./sdkconfig) and
recompile and reflash application.

### Interesting options

Most usefull option is `CONFIG_USE_MOCK_PUF` inside ESP-IDF config structure.

If macro is defined the mock implementation will be used.

When switching between mock and real implementations, `idf.py erase_flash` can be used
to erase all certificates / helper data.

You can configure log level by modifing ESP-IDF config. LEVEL 0 == NONE, LEVEL 5 == VERBOSE
```
CONFIG_LOG_DEFAULT_LEVEL_VERBOSE=n
...
CONFIG_LOG_DEFAULT_LEVEL_NONE=y
CONFIG_LOG_DEFAULT_LEVEL=0

CONFIG_LOG_MAXIMUM_LEVEL=0
```

## Structure

Main starting point is in [main.c](./main/main.c)

All the components including libraries are inside the [components](./components) directory.
