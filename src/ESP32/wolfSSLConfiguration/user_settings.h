// Mainly for debug outputs
#include "sdkconfig.h"

#define WOLFSSL_ESPIDF
#define WOLFSSL_ESPWROOM32

//#define PUF_DEBUG_SECRET // Disclose secret keys in my implementation

#define BENCH_EMBEDDED
#define USE_CERT_BUFFERS_2048
//#define WC_NO_CACHE_RESISTANT
//#define HAVE_COMP_KEY

// Certificate request generation
#define WOLFSSL_CERT_REQ
// Certificate generation
#define WOLFSSL_CERT_GEN
//#define WOLFSSL_SMALL_STACK_CACHE

// TLS 1.3
#define WOLFSSL_TLS13
#define HAVE_TLS_EXTENSIONS
#define WC_RSA_PSS
#define HAVE_HKDF
#define HAVE_AEAD
#define HAVE_SUPPORTED_CURVES
#define HAVE_ALPN

// Enable TLS_CHACHA20_POLY1305_SHA256
#define HAVE_CHACHA
#define HAVE_POLY1305
#define HAVE_ONE_TIME_AUTH
#define BUILD_TLS_CHACHA20_POLY1305_SHA256

// Enable ECC_BRAINPOOL
#define WOLFSSL_CUSTOM_CURVES
#define HAVE_ECC_BRAINPOOL


// Disable old ciphers
#define NO_DSA
#define NO_DH
#define NO_MD4
#define NO_DES3
#define NO_RC4
#define NO_RABBIT

/* Allows of x509 certs (for wolfssl_get_verify_result function) */
#define OPENSSL_EXTRA_X509_SMALL
#define WOLFSSL_BASE64_ENCODE

// Recude foodprint of cache
#define WOLFSSL_SMALL_STACK
#define SMALL_SESSION_CACHE

//#define SINGLE_THREADED
//#define NO_FILESYSTEM

#define HAVE_AESGCM
/* when you want to use SHA384 */
/* #define WOLFSSL_SHA384 */
#define WOLFSSL_SHA512
#define HAVE_ECC
#define HAVE_CURVE25519
#define CURVE25519_SMALL
#define HAVE_ED25519

/* debug options */
#define DEBUG_WOLFSSL
//#define DEBUG_WOLFSSL_VERBOSE
//#define SHOW_SECRETS
//#define WOLFSSL_DEBUG_ASN_TEMPLATE
//#define WOLFSSL_ESP32WROOM32_CRYPT_DEBUG
//#define WOLFSSL_ATECC508A_DEBUG

#define NO_MAIN_DRIVER

// Disables certificate time testing
#define NO_ASN_TIME
#define XTIME time
#define XGMTIME(c, t) gmtime((c))

/* when you want not to use HW acceleration */
#define NO_ESP32WROOM32_CRYPT
#define NO_WOLFSSL_ESP32WROOM32_CRYPT_HASH
/* #define NO_WOLFSSL_ESP32WROOM32_CRYPT_AES */
/* #define NO_WOLFSSL_ESP32WROOM32_CRYPT_RSA_PRI */
