%---------------------------------------------------------------
%---------------------------------------------------------------
\chapter{Transport Layer Security on ESP32}
%---------------------------------------------------------------
%---------------------------------------------------------------
\begin{chapterabstract}
  In this chapter, we explore different options to support
  Transport Layer Security (TLS) on ESP32.
  We weigh the positives and negatives of all options and then
  choose one that is the best match for our use case.
\end{chapterabstract}

%---------------------------------------------------------------
\section{Transport Layer Security}
%---------------------------------------------------------------
Transport Layer Security, commonly referred to as TLS, is a security protocol used to provide security in network transmissions.
It creates a secure connection between two parties across a computer network.
It can be used to encrypt any number of protocols (e.g.\ email, Voice over Internet Protocol) to securely transport information
across computer networks. TLS is best known for its use with Hypertext Transport Protocol (HTTP). Together they form Hypertext
Transport Protocol Secure (HTTPS) which is used all over Internet. The HTTPS protocol is used to encrypt communication between
web browser and web server, for example between you and your bank. \cite{cloudflare_what_is_tls}

Transport Layer Security is a evolution of a protocol known as Secure Sockets Layer (SSL). While the SSL protocols are
almost never used nowadays, the term SSL is still commonly used to refer to TLS. \cite{cloudflare_tls13}

%---------------------------------------------------------------
\subsection{Functions of TLS}
A Transport Layer Security is utilized for 3 main reasons \cite{cloudflare_what_is_tls}:
\begin{enumerate}
  \item \textit{encryption}: All communication inside TLS is encrypted to hide it from adversaries.
  \item \textit{authentication}: A mechanism exists to authenticate -- confirm that they are who they say they are --
            both sides of connection.
  \item \textit{integrity}: Any modifications and/or damage to the transmitted data can be detected.
\end{enumerate}

The encryption and integrity functions are simple to imagine, at least if you know how common encryption algorithms work.
However how do we authenticate someone across the network?

If we wish to authenticate using TLS we can use something called a \textit{certificate} \cite{cloudflare_what_is_tls}.
A certificate contains information (e.g.\ name, address, domain) and a public key of the subject.
Certificates are generally issued by certificate authority (CA). The CA signs the certificate using
asymmetric cryptography introduced in \autoref{subsec:asym_crypto}.
\cite{cloudflare_what_is_tls, cloudflare_certificate}

The CA guarantees that the information stored in the certificate is valid for the subject that has the private-public
key pair, from which the public part is stored inside the certificate. If we decide to trust the CA, we can authorize
the subject in the certificate.
\cite{cloudflare_what_is_tls, cloudflare_certificate}
% TODO this is rough


%---------------------------------------------------------------
\subsection{TLS Version 1.3}
Multiple versions of both SSL and TLS exist. A newest version\footnote{at the time of writing} is TLS 1.3
from August of 2018, standardized as RFC 8446. TLS 1.3 is a large step forward from the previous versions.
Features that are not commonly used or contain known vulnerabilities are removed to improve the security.
Faster and more effective mechanism to establish a secure connection is used. All with the aim to improve
performance, simplify the protocol and improve security. \cite{cloudflare_tls13}

In a recommendations published by Mozilla Organization \cite{mozilla_tls_versions} no versions before TLS 1.2 is recommended
to be used. If the service is expected to communicate with modern devices only and/or an high level of security is desired, TLS
version 1.3 should be used exclusively.

It must also be noted that another configuration, apart from version selection, should be applied. Namely the used ciphers and
authentication mechanisms should be limited to those with good support, security and no known vulnerabilities. \cite{mozilla_tls_versions}

Significant improvement in terms of security in TLS 1.3 is the mandatory use of forward secrecy (apart from few specific instances).
This change means that, even in the case of server private key disclosure to the adversary, no previous transmissions can
actually be decrypted by the adversary. To decrypt a transmissions a specific one time key needs to be discloses. Those keys
are however discarded after the communication ends. \cite{rfc8446_tls13}


%---------------------------------------------------------------
\section{Self Implementation of TLS}
%---------------------------------------------------------------
First idea might to simply implement Transport Layer Security on our own.
Let us suppose that we would like to implement TLS 1.3. The rationale behind this choice would be quite simple. It does
not make much sense to develop complicated library for a protocol that is already being replaced.

The starting point would be RFC 8446 \cite{rfc8446_tls13}, this document outlines all the required components that together
create Transport Layer Protocol version 1.3. It defines the handshake protocol, possible extensions, what cryptographic components
are used for authentication and encryption. It also references many different standards that we would need to implement also.

Any mistake, in any component, could lead to a security incident. In which private data might be disclosed, or a adversary
might take over the control of any device that is using our implementation. Even commonly used TLS libraries as OpenSSL
commonly suffer from vulnerabilities. Over the last 20 years that OpenSSL existed, over 200 vulnerabilities were found
\cite{openssl_vulnurabilities}.

Developing your own TLS library might certainly be possible, however the dangers and complexity it brings does not make
it practical. A possible compromise might be to create your own version of open source library (given that proper
license was followed), to customize the library for your purposes.

%---------------------------------------------------------------
\section{Available Libraries} \label{sec:tls}
%---------------------------------------------------------------
If we wish to use Transport Layer Security on ESP32, but we do not want to go to all the trouble of implementing our
own TLS library, we can turn to one of the already existing solutions.

Simple comparison between libraries can be found in \autoref{tab:tls_lib}.

%---------------------------------------------------------------
\subsection{ESP-TLS}
ESP-TLS is a component provided as part of ESP-IDF development framework by Espressif Systems (Shanghai) Co., Ltd \cite{esp_tls}.

It provides a very limited interface for Transport Layer Security. It uses Mbed SSL (default) or wolfSSL as underlying layer.  \cite{esp_tls}
This abstraction layer is available under Apache 2.0 license \cite{esp_idf_license}.

While this might be useful for simple applications that require TLS, and therefore do not use any special features,
for any more in-depth applications it does not provide much benefits.

%---------------------------------------------------------------
\subsection{WolfSSL}
WolfSSL \cite{wolfSSL_info} developed by wolfSSL Inc.\ is an open source TLS library targeted at low power and embedded devices.

The advantages highlighted by the developers include, but are not limited to: high portability, small size, support for
modern protocol and ciphers and Federal Information Processing Standards certifications. \cite{wolfSSL_info}

WolfSSL provides a comprehensive documentation both as learning materials and function documentation.
Both are accessible online, with support for generating your own via Doxygen.
Both forum and direct support contact are also provided. \cite{wolfSSL_docs}

%---------------------------------------------------------------
\subsection{Mbed TLS}
Mbed TLS developed by TrustedFirmware is a small TLS library. Library was previously known as PolarSSL and was maintained by
Arm. \cite{mbedTLS_info}

Since the inclusion of Mbed TLS in the Trusted Firmware project in 2020\footnote{almost 2 years at the time of writing} \cite{mbedTLS_joins_trusted_firmware},
a new major (and multiple minor) version were published. \cite{mbedTLS_versions}

\textit{``Mbed TLS provides a minimum viable implementation of the TLS 1.3 protocol''} \cite{mbedTLS_tls13}. The TLS 1.3 support
is not complete, however the set of implemented features is growing fast. \cite{mbedTLS_tls13}

The state of documentation for this library is less than ideal. Documentation hosted by Arm \cite{mbedTLS_arm_docs} is still
accessible, however the documented version (2.16.1) is quite behind the newest 3.1.0. For example the documented version does
not provide any TLS 1.3 support. On the other side, documentation hosted by Trusted Firmware
is \textit{still} not available \cite{mbedTLS_info}.

All the documentation is provided in the form of Doxygen, as part of the source code, it means that if we wish to have
up to date documentation available, we need to download the source code and generate it ourself. \cite{mbedTLS_readme}

%---------------------------------------------------------------
\subsection{BearSSL}
BearSSL developed by Thomas Pornin \cite{BearSSL} is a small TLS library aimed solely on small and embedded systems.

The library is published under MIT license and is still considered in \textit{beta}. It does however support client and server
side TLS 1.0, TLS 1.1 and TLS 1.2. Server and client side certificates are implemented including simple validation.
This library operates without the need for dynamic memory allocation. \cite{BearSSL}

A TLS 1.3 support is underway. However a estimated time of implementation is not provided. \cite{BearSSL_tls13}

%---------------------------------------------------------------
\subsection{CycloneSSL}
CycloneSSL developed by Oryx Embedded \cite{cycloneSSL} is a ANSI C compliant TLS library.

The library is published under GPLv2 license. It supports TLS versions 1.0 to 1.3 on both server and client with wide range
of ciphers to choose from (including Suite B). The library does not use any processor depended code. \cite{cycloneSSL}


\begin{table}[H]
  \begin{threeparttable}[b]
    \caption{Comparison between TLS libraries on ESP32}\label{tab:tls_lib}
    \begin{tabular}{|l|c|c|c|c|}\hline
        Library & License\tnote{a} & TLS support & Certifications & Days since last release\tnote{b}\\\hline\hline
        wolfSSL & GPLv2 & 1.3 & DO-178\tnote{c}, FIPS 140-2\tnote{c} & 2\\
        Mbed TLS & Apache 2.0 & experimental 1.3 & No\tnote{d} & 139\\
        BearSSL & MIT & 1.2 & No & 1360\\
        CycloneSSL & GPLv2 & 1.3 & No & 92\\\hline
    \end{tabular}
    \begin{tablenotes}
      \item [a] Commercial licenses are also available.
      \item [b] As of 2022-05-05. \cite{wolfSSL_versions, mbedTLS_versions, BearSSL, cycloneSSL_release}
      \item [c] Only for wolfCrypt component. \cite{wolfSSL_audit}
      \item [d] The official FIPS 140-2 tests are included in automatic testing. \cite{mbedTLS_audit}
    \end{tablenotes}
  \end{threeparttable}
\end{table}

%---------------------------------------------------------------
\section{The Best TLS Library?} \label{sec:tls_choise}
%---------------------------------------------------------------
Based on the information we gathered in \autoref{sec:tls} we carefully considered our choice for TLS library.

While all options would be potentially suitable, we want to target TLS version 1.3 to demonstrate the usability in modern protocols. This
allows this work to remain relevant for years to come. Another point to consider it a license. We wanted to use an open source library,
however the license does not make much of a difference for us.

\begin{description}
 \item[wolfSSL] is quite popular on the ESP32 platform. This is important in the development phase, since many resources are available.
        It also supports a TLS 1.3.
 \item[Mbed TLS] is probably the most popular on ESP32 (maybe due to its default inclusion in ESP-TLS). However its support of TLS 1.3
        is somewhat lacking. It is also released under the Apache 2.0 license.
 \item[BearSSL] does not unfortunately support TLS 1.3. The development is also rather slow. However it is released under MIT license
        if that is something that your project requires.
 \item[CycloneSLL] does support TLS 1.3 and is released under GPLv2 license.
\end{description}

Mbed TLS might be a suitable open source option, if a GPLv2 license, or other commercial license options, do not suit your needs.
However we decided to use \textit{wolfSSL} for the TLS 1.3 support, active development and support it offers.

%---------------------------------------------------------------
%---------------------------------------------------------------
\chapter{Design of a Key Enrollment Procedure} \label{chap:enrollment}
%---------------------------------------------------------------
%---------------------------------------------------------------
\begin{chapterabstract}
  In this chapter, we design a key enrollment procedure. This allows
  us to use private keys, that were generated on the device, within TLS
  to facilitate authentication.
\end{chapterabstract}

%Design and implement key enrollment procedure for key certifications
Transport Layer Security encrypts traffic between two parties with the aim of creating a secure communication
channel. It also support an authorization using TLS certificates.

We designed a key enrollment procedure to allow us to authorize the device, within already established system,
using industry standard certificate mechanisms. The device creates a certificate signing request (CSR)
that can be used to receive a certificate from a certificate authority.

The EPS32 is generally not used as end user device and thus do not posses any human usable interface
(e.g.\ keyboard, screen). For this reason, another device is needed to ``talk'' with the device.
While we intended for the device to be a computer, the protocol is device independent.


%---------------------------------------------------------------
\section{Aims of the Procedure}
%---------------------------------------------------------------
When designing the procedure we wanted to keep it simple.

Simple means easier implementation, which in turn leads to safer and less error prone code.
While more sophisticated procedure could implement a error recovery or modification subroutines
we did not think that such features would provide significant benefit.

The enrollment procedure is most likely done only once (over the life of the device). And since it is not
a complicated process, it does not, in our opinion, warrant the increased complexity that would additional
features bring. If at any phase a error occurs, it is simply a matter of resetting the device and
repeating the procedure.

%---------------------------------------------------------------
\section{Communication Medium}
%---------------------------------------------------------------
The ESP32 platform itself offers multiple possible communication mediums.

There are possibly 3 main options: Bluetooth, Wi-Fi, and Universal asynchronous receiver-transmitter (UART)
\cite{esp_datasheet}. Other options exist, but most of them would require some development work and/or
additional hardware.

A care must be taken to secure this medium against a man in the middle attack. If an adversary takes over
the communication, he can create his own certificate signing request. If this CSR then gets signed it allows
the adversary to impersonate the device. While this device would be then unusable (the devices public keys
would not match the public key stored inside the certificate), it still allows the adversary to have
a valid certificate.

We therefore recommend the use of a UART interface. The interface is accessible only locally, so the
adversary would require a physical access (or a compromised computer on the other side) to the device
at the time of the enrollment.

A significant advantage in favor of UART is the presence of a USB to serial converter on most ESP32 development
boards. This is mainly useful in the development phase. However a single affordable external USB to serial
converter can be used for numerous deployment, leading to no significant additional cost for a manufactured unit.
However, as noted previously, this enrollment procedure can be -- with sufficient precautions -- implemented
over any medium of choice.


%---------------------------------------------------------------
\section{Procedure}
%---------------------------------------------------------------
You can find a diagram of the procedure in \autoref{fig:enrollment}. In the following subsection
we describe each phase in depth.


Since the aim of the procedure was to be simple, we did not want to require and significant software
on the opposite computer side. We therefore decided to transfer all information as in text form. This way
any serial monitor application could be used to enroll a device.

The format of the messages send from a device to a computer is inspired by the Privacy-Enhanced Mail (PEM)
encoding format \cite{rfc7468_pem}, that is commonly used with certificates and private keys. Every
message is ended by a new line character ('\textbackslash{}n').
The messages from the computer to the device are of two types. In the first section of the procedure
a simple textual strings are transmitted. In the last section, an actual PEM encoded certificates are transmitted.

We call the single line messages from the device to the computer a header. All the headers look like this:
``-{}-{}-{}-{}- \{PLACEHOLDER\} -{}-{}-{}-{}-''. Where ``\{PLACEHOLDER\}'' is a variable length string
of upper and lower case letters, numbers, spaces, and underscores.

All messages transmitted by the device are using an 8-bit ASCII encoding.

If at any stage in the enrollment procedure the devices encounters an error. It signals it via a
``-{}-{}-{}-{}-FAIL ENROLLMENT-{}-{}-{}-{}-'' header and terminates the procedure.

An example of full enrollment procedure between the device and the computer can be found in \autoref{app:enrollment}.

%---------------------------------------------------------------
\subsection{Triggering Key Enrollment}
For a security reasons the enrollment procedure must be triggered manually on the device.
It is intended as a countermeasure to a remote denial of service. We suggest that a
push button is installed, which is required to be pressed at the device startup, to enter the enroll
procedure.

The device establishes connection across the selected medium (e.g.\ UART serial communication)
and sends ``-{}-{}-{}-{}-BEGIN ENROLLMENT-{}-{}-{}-{}-'' header to signalize the start of the
enrollment procedure.

%---------------------------------------------------------------
\subsection{Creating Certificate Signing Request}
Now that the enrollment procedure is started, we need to get information from the computer to create the
certificate signing request. Each CSR consists of multiple fields that need to be filled with information.
The fields include identification information (e.g.\ country, organization, common name).

The device will send headers in the form of ``-{}-{}-{}-{}-INPUT \{PLACEHOLDER\}-{}-{}-{}-{}-''
where ``\{PLACEHOLDER\}'' is replaced with the requested field name. The field names adhere to ``LDAP-NAME''
of attributes defined in Section 6 of X.520 \cite{x520_cert} recommendation. If multiple versions
are defined a longer version is used. All names are converted to upper case letters before usage.
The order of fields is consistent with the ordering of X.520 \cite{x520_cert}.

If any additional fields are supported by the device, they are ordered lexicographically, using the most
common field names, and filled last.

For example if fields country name, common name, and custom are supported, the headers are ordered as follows:
\begin{enumerate}
  \item ``-{}-{}-{}-{}-INPUT COMMONNAME-{}-{}-{}-{}-'' as common name is defined first in Section 6.2.2 with
    two possible names (longer is used and capitalized).
  \item ``-{}-{}-{}-{}-INPUT C-{}-{}-{}-{}-'' as country name is defined in Section 6.3.1.
  \item ``-{}-{}-{}-{}-INPUT CUSTOM-{}-{}-{}-{}-'' as custom is not part of the recommendation.
\end{enumerate}

After the header is sent, the device awaits a response. If the response is only an empty line that means
that the field should remain unused.

After the response have been received it is validated against requirements defined in X.520 \cite{x520_cert}
(mainly maximal length and encoding). The process repeats until all supported fields are filled.

%---------------------------------------------------------------
\subsection{Exporting Certificate Signing Request}
After all fields of the certificate signing request are filled the device assembles and completes the CSR.
The fully prepared CSR is then transmitted as a PEM encoded multiline message.
Afterwards all currently stored certificates are deleted from the system.

The message is, in accordance with RFC~7468 \cite{rfc7468_pem}, started with header
``-{}-{}-{}-{}-BEGIN CERTIFICATE REQUEST-{}-{}-{}-{}-'' and ended with header
``-{}-{}-{}-{}-END CERTIFICATE REQUEST-{}-{}-{}-{}-''.

This certificate signing request can then be transmitted to certificate authority of choice. The CA then should
perform signing a certificate with the rest of certificate chain of trust.

%---------------------------------------------------------------
\subsection{Receiving Certificate Chain}
The device prompts for a count of certificates in the chain with header ``-{}-{}-{}-{}-INPUT CERTIFICATE COUNT-{}-{}-{}-{}-''.
An positive whole number, larger than 0, is expected as a response.

A device now sends a header ``-{}-{}-{}-{}-INPUT CERTIFICATE CHAIN-{}-{}-{}-{}-'' prompting the computer
to send the certificate chain. The certificate chain should consist of PEM encoded certificates ordered
from the devices certificate to the root certificate.

Device reads all the certificates, the count was specified previously with the response to the INPUT CERTIFICATE
COUNT message, and saves them for future use within TLS.

%---------------------------------------------------------------
\subsection{Validating Certificate Chain}
Last part of the enrollment procedure is for the device validate that the certificate was intended for this device,
a devices public key stored in the certificate should therefore be validated against devices private key.

If all the steps leading up to here and the public key check were completed successfully, the device sends
last header ``-{}-{}-{}-{}-END ENROLLMENT-{}-{}-{}-{}-'' to signalize a successful enrollment.

\begin{figure}[!p]\centering
    \includegraphics[width=\textwidth]{text/img/enrollment}
    \caption{Enrollment activity diagram}\label{fig:enrollment}
\end{figure}
