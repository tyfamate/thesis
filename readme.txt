############################################################################################
# Bachelor's thesis
############################################################################################

Using physical unclonable functions in TLS on ESP32

Matěj Týfa
CVUT 2022

############################################################################################
# Contents of this CD
############################################################################################

readme.txt .....................................introduction and information about the media
src
    ESP32 ..................................source code for the proof of concept application
        components
            puflib.................................esp32_puflib developed by Ondřej Staníček
            wolfssl................................wolfSSL library developed by wolfSSL Inc.
        main .....................................................application starting point
        wolfSSLConfiguration ..........................configuration for the wolfSSL library
        sdkconfig.defaults.........................default configuration for the application
    PC..............................................................enrollment helper script
    thesis ................................................LATEX source code for this thesis
generated
    thesis.pdf....................................................PDF version of this thesis

############################################################################################

The src directory contains source codes. It is sepatated into ESP32 (the application), PC (helper scripts) and thesis (the thesis LaTeX source code).
More information about builing, usage, and configuration are in their respective folder.
